﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Scaffold
{
    // SOURCE:
    // http://stackoverflow.com/questions/10320232/how-to-accept-a-file-post-asp-net-mvc-4-webapi
    // http://www.strathweb.com/2012/08/a-guide-to-asynchronous-file-uploads-in-asp-net-web-api-rtm/
    public class UploadController : ApiController
    {
        protected readonly string UploadFolder;

        public UploadController() : base() { }

        public UploadController(string uploadFolder)
        {
            UploadFolder = uploadFolder;
        }

        public Task<IEnumerable<FileDesc>> PostFile()
        {
            if (!Request.Content.IsMimeMultipartContent())
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.UnsupportedMediaType));

            var root = HttpContext.Current.Server.MapPath("/Content/temp/" + UploadFolder);
            var provider = new GuidMultipartFormDataStreamProvider(root);

            var task = Request.Content.ReadAsMultipartAsync(provider).ContinueWith<IEnumerable<FileDesc>>(t =>
            {
                if (t.IsFaulted || t.IsCanceled)
                {
                    Request.CreateErrorResponse(HttpStatusCode.InternalServerError, t.Exception);
                }

                var result = provider.FileData.Select(i =>
                {
                    var info = new FileInfo(i.LocalFileName);
                    return new FileDesc(
                        info.Name,
                        i.Headers.ContentType.MediaType,
                        "/Content/temp/" + UploadFolder + info.Name,
                        info.Length / 1024);
                });

                BeforeResponse(result);
                return result;
            });

            return task;
        }

        [HttpDelete]
        public IHttpActionResult DeleteFile(string fileNames)
        {
            try
            {
                var file = HttpContext.Current.Server.MapPath(fileNames);
                if (File.Exists(file))
                {
                    File.Delete(file);

                    return Ok(true);
                }

                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        protected virtual void BeforeResponse(IEnumerable<FileDesc> fileDescs) { }
    }

    public class FileDesc
    {
        public FileDesc(string name, string type, string path, long size)
        {
            Name = name;
            Type = type;
            Path = path;
            Size = size;
        }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Path { get; set; }
        public long Size { get; set; }
    }

    public class GuidMultipartFormDataStreamProvider : MultipartFormDataStreamProvider
    {
        public GuidMultipartFormDataStreamProvider(string path) : base(path) { }

        public override string GetLocalFileName(System.Net.Http.Headers.HttpContentHeaders headers)
        {
            string filename = headers.ContentDisposition.FileName.Replace("\"", "");
            return Guid.NewGuid().ToString("N") + Path.GetExtension(filename); // N = 32 digits no hyphens;
        }
    }
}
