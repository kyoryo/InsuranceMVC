﻿using System;
using System.IO;
using System.Web;
using System.Web.Http;

namespace Scaffold
{
    // SOURCE:
    // http://stackoverflow.com/questions/10320232/how-to-accept-a-file-post-asp-net-mvc-4-webapi
    // http://www.strathweb.com/2012/08/a-guide-to-asynchronous-file-uploads-in-asp-net-web-api-rtm/
    public class DeleteFileController : ApiController
    {
        public IHttpActionResult PostDelete(string fileNames)
        {
            try
            {
                var file = HttpContext.Current.Server.MapPath(fileNames);
                if (File.Exists(file))
                {
                    File.Delete(file);

                    return Ok(true);
                }

                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
