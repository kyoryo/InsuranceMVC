﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Helpers;
using System.Web.Mvc;
using Newtonsoft.Json;
using Scaffold;
using Scaffold.Models.Interface;

namespace Domain.Infrastructure
{
    [TrackChanges()]
    public class Menu : IModel<Guid>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        [Index("IX_Menu_Name_Unique", IsUnique = true)]
        public string Name { get; set; }
        public string Title { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
        public string Url { get; set; }
        public string Icon { get; set; }

        [Column("SubMenu")]
        public string SubMenuJson { get; set; }

        [NotMapped]
        [UIHint("SubMenu")]
        [DisplayName("Sub Menu")]
        public virtual IList<SubMenu> SubMenu {
            get
            {
                return string.IsNullOrWhiteSpace(SubMenuJson) ? null : Json.Decode<List<SubMenu>>(SubMenuJson);
            }
            set
            {
                SubMenuJson = Json.Encode(value);
            } 
        }

        // Skip this field from audit trail
        [SkipTracking()]
        [Required]
        [ToTS(TSFlag.SkipCRUD)]
        public DateTime CreatedAt { get; set; }

        // Skip this field from audit trail
        [SkipTracking()]
        [HiddenInput]
        [ToTS(TSFlag.SkipCRUD)]
        public DateTime? LastUpdateTime { get; set; }

        public int SortOrder { get; set; }

        #region Relationship

        [JsonIgnore]
        [ToTS(TSFlag.Ignore)]
        public virtual IList<RoleMenu> RoleMenus { get; set; }

        #endregion
    }

    public class SubMenu
    {
        [Required]
        public string Name { get; set; }
        public string Title { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
        public string Url { get; set; }
        public string Icon { get; set; }
        public int SortOrder { get; set; }
    }
}
