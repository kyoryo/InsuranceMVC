﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using Scaffold;
using Scaffold.Models.Interface;

namespace Domain.Models
{
    [TrackChanges()]
    public class Category : IModel<int>, ISoftDeletable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [AllowHtml]
        [UIHint("RichTextBox")]
        [ToTS(TSFlag.SkipIndex)]
        public string Description { get; set; }

        // Skip this field from audit trail
        [SkipTracking()]
        [Required]
        [ToTS(TSFlag.SkipCRUD)]
        public DateTime CreatedAt { get; set; }

        // Skip this field from audit trail
        [SkipTracking()]
        [ToTS(TSFlag.SkipCRUD)]
        public DateTime? LastUpdateTime { get; set; }

        [ToTS(TSFlag.Ignore)]
        public virtual IList<Product> Products { get; set; }

        [ToTS(TSFlag.Ignore)]
        public bool IsDeleted { get; set; }
    }
}