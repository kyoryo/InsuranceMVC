﻿using Scaffold;
using Scaffold.Models.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Domain.Models
{
    public class KerjaKlaim : IModel<int>, ISoftDeletable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        #region log
        [SkipTracking()]
        [Required]
        [ToTS(TSFlag.SkipCRUD)]
        public DateTime CreatedAt { get; set; }
        [SkipTracking()]
        [HiddenInput]
        [ToTS(TSFlag.SkipCRUD)]
        public DateTime? LastUpdateTime { get; set; }
        [ToTS(TSFlag.Ignore)]
        public bool IsDeleted { get; set; }
        #endregion

        [Required]
        [DisplayName("Kerja Klaim")]
        public string Name { get; set; }
    }
}
