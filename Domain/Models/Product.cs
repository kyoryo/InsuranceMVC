﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using Scaffold;
using Scaffold.Models.Interface;

namespace Domain.Models
{
    [TrackChanges()]
    public class Product : IModel<Guid>, ISoftDeletable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        [AllowHtml]
        [UIHint("MultiFileUpload")]
        [ToTS(TSFlag.SkipIndex)]
        public string Picture { get; set; }

        [AllowHtml]
        [UIHint("RichTextBox")]
        [ToTS(TSFlag.SkipIndex)]
        public string Description { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Required]
        public int Quantity { get; set; }

        [Required]
        public int CategoryId { get; set; }

        [ToTS(TSFlag.Ignore)]
        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }

        // Skip this field from audit trail
        [SkipTracking()]
        [Required]
        [ToTS(TSFlag.SkipCRUD)]
        public DateTime CreatedAt { get; set; }

        // Skip this field from audit trail
        [SkipTracking()]
        [HiddenInput]
        [ToTS(TSFlag.SkipCRUD)]
        public DateTime? LastUpdateTime { get; set; }

        [ToTS(TSFlag.Ignore)]
        public bool IsDeleted { get; set; }
    }
}