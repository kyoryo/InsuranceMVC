﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Models
{
    public class Attachment
    {
        [Required]
        public string Name { get; set; }
        public string FileType { get; set; }
        public string Type { get; set; }
        public double Size { get; set; }
        public string Path { get; set; }
    }

    public enum AttachmentType
    {
        File,
        Url
    }
}
