﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Infrastructure;
using Scaffold;
using Scaffold.Models.Interface;

namespace Domain.Models
{
    [TrackChanges()]
    public class UserProfile : IModel<int>
    {
        public int Id { get; set; }

        [Required]
        [DisplayName("Nama")]
        [MaxLength(200)]
        public string Name { get; set; }

        [DisplayName("Tempat Lahir")]
        public string Birthplace { get; set; }

        [DisplayName("Tanggal Lahir")]
        public DateTime? Birthdate { get; set; }

        [Required]
        [DisplayName("Alamat")]
        public string Address { get; set; }

        [DisplayName("Foto")]
        public string Photo { get; set; }

        [SkipTracking()]
        public DateTime CreatedAt { get; set; }

        [SkipTracking()]
        public DateTime? LastUpdateTime { get; set; }

        #region Relationship

        [NotMapped]
        public virtual ApplicationUser ApplicationUser { get; set; }

        #endregion

        [ToTS(TSFlag.Ignore)]
        public override string ToString()
        {
            return Name;
        }
    }
}
