﻿using Scaffold;
using Scaffold.Models.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Domain.Models
{
    [TrackChanges()]
    public class Derek : IModel<int>, ISoftDeletable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [DisplayName("Nama Pelapor")]
        public string Name { get; set; }
        [Required]
        [DisplayName("Nomor Telpon")]
        public string Telephone { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }
        [Required]
        [DisplayName("Tanggal Kejadian")]
        public DateTime EventDate { get; set; }
        [Required]
        [DisplayName("Waktu Kejadian")]
        public DateTime EventTime { get; set; }

        [Required]
        [DisplayName("Lokasi")]
        public string Location { get; set; }
        [DisplayName("Kota Kejadian")]
        public int CityId { get; set; }
        [DisplayName("Keterangan")]
        public string Description { get; set; }

        #region foreign field
        
        public int? KerjaDerekId { get; set; }
        
        public int? PemegangPolisId { get; set; }
        #endregion

        #region log
        // Skip this field from audit trail
        [SkipTracking()]
        [Required]
        [ToTS(TSFlag.SkipCRUD)]
        public DateTime CreatedAt { get; set; }
        
        // Skip this field from audit trail
        [SkipTracking()]
        [HiddenInput]
        [ToTS(TSFlag.SkipCRUD)]
        public DateTime? LastUpdateTime { get; set; }

        [ToTS(TSFlag.Ignore)]
        public bool IsDeleted { get; set; }
        #endregion

        #region relationship
        [ForeignKey("KerjaDerekId")]
        public virtual KerjaDerek KerjaDerek { get; set; }
        [ForeignKey("PemegangPolisId")]
        public virtual PemegangPolis PemegangPolis { get; set; }
        [ForeignKey("CityId")]
        public virtual City City { get; set; }
        #endregion
    }
}
