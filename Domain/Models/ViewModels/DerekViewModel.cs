﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using Heroic.AutoMapper;

namespace Domain.Models.ViewModels
{
    public class DerekViewModel : IMapFrom<Derek>, IHaveCustomMappings
    {
        public City City { get; set; }
        public KerjaDerek KerjaDerek { get; set; }
        public PemegangPolis PemegangPolis { get; set; }
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Telephone { get; set; }
        public string Email { get; set; }
        [Required]
        public DateTime EventDate { get; set; }
        [Required]
        public DateTime EventTime { get; set; }
        [Required]
        public string Location { get; set; }
        public int CityId { get; set; }
        public string Description { get; set; }
        public int? KerjaDerekId { get; set; }
        public int? PemegangPolisId { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
        public DateTime? LastUpdateTime { get; set; }
        
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<Derek, DerekViewModel>();
        }

        public virtual Derek ToEntity()
        {
            return new Derek()
            {
                Id = Id,
                Name = Name,
                Telephone = Telephone,
                Email = Email,
                EventDate = EventDate,
                EventTime = EventTime,
                Location = Location,
                CityId = CityId,
                Description = Description,
                KerjaDerekId = KerjaDerekId,
                PemegangPolisId = PemegangPolisId,
                CreatedAt = CreatedAt,
                LastUpdateTime = LastUpdateTime,
            };
        }
    }
}

