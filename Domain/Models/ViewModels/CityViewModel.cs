﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using Heroic.AutoMapper;

namespace Domain.Models.ViewModels
{
    public class CityViewModel : IMapFrom<City>, IHaveCustomMappings
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
        public DateTime? LastUpdateTime { get; set; }
        
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<City, CityViewModel>();
        }

        public virtual City ToEntity()
        {
            return new City()
            {
                Id = Id,
                Name = Name,
                CreatedAt = CreatedAt,
                LastUpdateTime = LastUpdateTime,
            };
        }
    }
}

