﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using Heroic.AutoMapper;
using Domain.Infrastructure;

namespace Domain.Models.ViewModels
{
    public class PemegangPolisViewModel : IMapFrom<PemegangPolis>, IHaveCustomMappings
    {
        public ApplicationUser Owner { get; set; }
        public int Id { get; set; }
        public string OwnerId { get; set; }
        public string NomorPolis { get; set; }
        public string NomorMesin { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
        public DateTime? LastUpdateTime { get; set; }
        
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<PemegangPolis, PemegangPolisViewModel>();
        }

        public virtual PemegangPolis ToEntity()
        {
            return new PemegangPolis()
            {
                Id = Id,
                OwnerId = OwnerId,
                NomorPolis = NomorPolis,
                NomorMesin = NomorMesin,
                CreatedAt = CreatedAt,
                LastUpdateTime = LastUpdateTime,
            };
        }
    }
}

