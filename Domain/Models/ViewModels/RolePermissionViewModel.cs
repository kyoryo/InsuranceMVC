﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using Domain.Infrastructure;
using Heroic.AutoMapper;

namespace Domain.Models.ViewModels
{
    public class RolePermissionViewModel : IMapFrom<RolePermission>, IHaveCustomMappings
    {
        public Permission Permission { get; set; }
        public ApplicationRole Role { get; set; }
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? LastUpdateTime { get; set; }
        [Required]
        public string RoleId { get; set; }
        [Required]
        public Guid PermissionId { get; set; }
        
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<RolePermission, RolePermissionViewModel>();
        }

        public virtual RolePermission ToEntity()
        {
            return new RolePermission()
            {
                Id = Id,
                CreatedAt = CreatedAt,
                LastUpdateTime = LastUpdateTime,
                RoleId = RoleId,
                PermissionId = PermissionId,
            };
        }
    }
}

