﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using Heroic.AutoMapper;

namespace Domain.Models.ViewModels
{
    public class BranchOfficeViewModel : IMapFrom<BranchOffice>, IHaveCustomMappings
    {
        public City City { get; set; }
        public int Id { get; set; }
        public string Address { get; set; }
        public int CityId { get; set; }
        public string Telephone { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
        public DateTime? LastUpdateTime { get; set; }
        
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<BranchOffice, BranchOfficeViewModel>();
        }

        public virtual BranchOffice ToEntity()
        {
            return new BranchOffice()
            {
                Id = Id,
                Address = Address,
                CityId = CityId,
                Telephone = Telephone,
                CreatedAt = CreatedAt,
                LastUpdateTime = LastUpdateTime,
            };
        }
    }
}

