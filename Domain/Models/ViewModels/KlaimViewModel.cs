﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using Heroic.AutoMapper;

namespace Domain.Models.ViewModels
{
    public class KlaimViewModel : IMapFrom<Klaim>, IHaveCustomMappings
    {
        public KerjaKlaim KerjaKlaim { get; set; }
        public PemegangPolis PemegangPolis { get; set; }
        public int Id { get; set; }
        [Required]
        public string NamaPengemudi { get; set; }
        [Required]
        public string NomorTelpon { get; set; }
        public string Email { get; set; }
        [Required]
        public DateTime TanggalKejadian { get; set; }
        [Required]
        public DateTime WaktuKejadian { get; set; }
        [Required]
        public string LokasiKejadian { get; set; }
        [Required]
        public string Keterangan { get; set; }
        public int? KerjaKlaimId { get; set; }
        public int? PemegangPolisId { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
        public DateTime? LastUpdateTime { get; set; }
        
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<Klaim, KlaimViewModel>();
        }

        public virtual Klaim ToEntity()
        {
            return new Klaim()
            {
                Id = Id,
                NamaPengemudi = NamaPengemudi,
                NomorTelpon = NomorTelpon,
                Email = Email,
                TanggalKejadian = TanggalKejadian,
                WaktuKejadian = WaktuKejadian,
                LokasiKejadian = LokasiKejadian,
                Keterangan = Keterangan,
                KerjaKlaimId = KerjaKlaimId,
                PemegangPolisId = PemegangPolisId,
                CreatedAt = CreatedAt,
                LastUpdateTime = LastUpdateTime,
            };
        }
    }
}

