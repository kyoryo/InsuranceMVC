﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using Heroic.AutoMapper;

namespace Domain.Models.ViewModels
{
    public class CategoryViewModel : IMapFrom<Category>, IHaveCustomMappings
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
        public DateTime? LastUpdateTime { get; set; }
        
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<Category, CategoryViewModel>();
        }

        public virtual Category ToEntity()
        {
            return new Category()
            {
                Id = Id,
                Name = Name,
                Description = Description,
                CreatedAt = CreatedAt,
                LastUpdateTime = LastUpdateTime,
            };
        }
    }
}

