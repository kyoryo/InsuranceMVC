﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using Heroic.AutoMapper;

namespace Domain.Models.ViewModels
{
    public class BengkelRekananViewModel : IMapFrom<BengkelRekanan>, IHaveCustomMappings
    {
        public City City { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string ContactPerson { get; set; }
        public int CityId { get; set; }
        public string Description { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
        public DateTime? LastUpdateTime { get; set; }
        
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<BengkelRekanan, BengkelRekananViewModel>();
        }

        public virtual BengkelRekanan ToEntity()
        {
            return new BengkelRekanan()
            {
                Id = Id,
                Name = Name,
                Address = Address,
                Telephone = Telephone,
                Fax = Fax,
                Email = Email,
                ContactPerson = ContactPerson,
                CityId = CityId,
                Description = Description,
                CreatedAt = CreatedAt,
                LastUpdateTime = LastUpdateTime,
            };
        }
    }
}

