﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using Heroic.AutoMapper;

namespace Domain.Models.ViewModels
{
    public class KerjaKlaimViewModel : IMapFrom<KerjaKlaim>, IHaveCustomMappings
    {
        public int Id { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
        public DateTime? LastUpdateTime { get; set; }
        [Required]
        public string Name { get; set; }
        
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<KerjaKlaim, KerjaKlaimViewModel>();
        }

        public virtual KerjaKlaim ToEntity()
        {
            return new KerjaKlaim()
            {
                Id = Id,
                CreatedAt = CreatedAt,
                LastUpdateTime = LastUpdateTime,
                Name = Name,
            };
        }
    }
}

