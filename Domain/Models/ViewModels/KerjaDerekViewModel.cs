﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using Heroic.AutoMapper;

namespace Domain.Models.ViewModels
{
    public class KerjaDerekViewModel : IMapFrom<KerjaDerek>, IHaveCustomMappings
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
        public DateTime? LastUpdateTime { get; set; }
        
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<KerjaDerek, KerjaDerekViewModel>();
        }

        public virtual KerjaDerek ToEntity()
        {
            return new KerjaDerek()
            {
                Id = Id,
                Name = Name,
                CreatedAt = CreatedAt,
                LastUpdateTime = LastUpdateTime,
            };
        }
    }
}

