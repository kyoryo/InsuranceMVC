﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using Domain.Infrastructure;
using Heroic.AutoMapper;

namespace Domain.Models.ViewModels
{
    public class MenuViewModel : IMapFrom<Menu>, IHaveCustomMappings
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Title { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
        public string Url { get; set; }
        public string Icon { get; set; }
        public string SubMenuJson { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
        public DateTime? LastUpdateTime { get; set; }
        public int SortOrder { get; set; }
        
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<Menu, MenuViewModel>();
        }

        public virtual Menu ToEntity()
        {
            return new Menu()
            {
                Id = Id,
                Name = Name,
                Title = Title,
                Action = Action,
                Controller = Controller,
                Url = Url,
                Icon = Icon,
                SubMenuJson = SubMenuJson,
                CreatedAt = CreatedAt,
                LastUpdateTime = LastUpdateTime,
                SortOrder = SortOrder,
            };
        }
    }
}

