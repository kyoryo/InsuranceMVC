﻿using System;

namespace Domain.Models.Interface
{
    public interface IBaseUser
    {
        string Id { get; set; }
        string UserName { get; set; }
        string Email { get; set; }
        string Name { get; set; }
        string Telephone { get; set; }
        string Address { get; set; }
        string BirthPlace { get; set; }
        DateTime? BirthDate { get; set; }
        string Photo { get; set; }
        string Status { get; set; }
    }
}