﻿using Scaffold;
using Scaffold.Models.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Domain.Models
{
    public class Klaim : IModel<int>, ISoftDeletable
    {
        //klaim step 1
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [DisplayName("Nama Pengemudi")]
        public string NamaPengemudi { get; set; }
        [Required]
        [DisplayName("Nomor Telpon")]
        public string NomorTelpon { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }
        [Required]
        [DisplayName("Tanggal Kejadian")]
        public DateTime TanggalKejadian { get; set; }
        [Required]
        [DisplayName("Waktu Kejadian")]
        public DateTime WaktuKejadian { get; set; }
        [Required]
        [DisplayName("Lokasi Kejadian")]
        public string LokasiKejadian { get; set; }
        [Required]
        public string Keterangan { get; set; }

        #region foreign field
        [DisplayName("Kerja Klaim")]
        public int? KerjaKlaimId { get; set; }
        public int? PemegangPolisId { get; set; }        
        #endregion

        // Skip this field from audit trail
        [SkipTracking()]
        [Required]
        [ToTS(TSFlag.SkipCRUD)]
        public DateTime CreatedAt { get; set; }

        // Skip this field from audit trail
        [SkipTracking()]
        [HiddenInput]
        [ToTS(TSFlag.SkipCRUD)]
        public DateTime? LastUpdateTime { get; set; }

        [ToTS(TSFlag.Ignore)]
        public bool IsDeleted { get; set; }

        #region relationship
        [ForeignKey("KerjaKlaimId")]
        public virtual KerjaKlaim KerjaKlaim { get; set; }
        [ForeignKey("PemegangPolisId")]
        public virtual PemegangPolis PemegangPolis { get; set; }
        #endregion
    }
}
