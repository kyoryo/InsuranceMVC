﻿using Scaffold;
using Scaffold.Models.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Domain.Models
{
    [TrackChanges()]
    public class BranchOffice : IModel<int>, ISoftDeletable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [DisplayName("Alamat")]
        public string Address { get; set; }
        [DisplayName("Kota")]
        public int CityId { get; set; }
        [DisplayName("Nomor Telpon")]
        public string Telephone { get; set; }

        #region log
        // Skip this field from audit trail
        [SkipTracking()]
        [Required]
        [ToTS(TSFlag.SkipCRUD)]
        public DateTime CreatedAt { get; set; }

        // Skip this field from audit trail
        [SkipTracking()]
        [HiddenInput]
        [ToTS(TSFlag.SkipCRUD)]
        public DateTime? LastUpdateTime { get; set; }

        [ToTS(TSFlag.Ignore)]
        public bool IsDeleted { get; set; }
        #endregion

        #region relationships
        [ForeignKey("CityId")]
        public virtual City City { get; set; }
        #endregion
    }
}
