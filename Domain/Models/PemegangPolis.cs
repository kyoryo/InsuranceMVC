﻿using Domain.Infrastructure;
using Scaffold;
using Scaffold.Models.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Domain.Models
{
    [TrackChanges()]
    public class PemegangPolis : IModel<int>, ISoftDeletable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string OwnerId {get;set;}
        [Required]
        //[Index("IX_Menu_Name_Unique", IsUnique = true)]
        public string NomorPolis { get; set; }
        [Required]
        //[Index("IX_Menu_Name_Unique", IsUnique = true)]
        public string NomorMesin { get; set; }

        //public int KlaimId { get; set; }

        // Skip this field from audit trail
        [SkipTracking()]
        [Required]
        [ToTS(TSFlag.SkipCRUD)]
        public DateTime CreatedAt { get; set; }

        // Skip this field from audit trail
        [SkipTracking()]
        [HiddenInput]
        [ToTS(TSFlag.SkipCRUD)]
        public DateTime? LastUpdateTime { get; set; }

        [ToTS(TSFlag.Ignore)]
        public bool IsDeleted { get; set; }

        #region relationship

        [ForeignKey("OwnerId")]
        public virtual ApplicationUser Owner { get; set; }
        //public virtual Klaim Klaim { get; set; }
        #endregion


    }
}
