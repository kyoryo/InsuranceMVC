﻿using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace App.Helper
{
    public static class HtmlHelpers
    {
        public static MvcHtmlString MenuLink(this HtmlHelper htmlHelper, string linkText, string actionName,
            string controllerName, string iconClass, bool isRootLevel = false)
        {
            var builder = new TagBuilder("li");

            var currentAction = htmlHelper.ViewContext.ParentActionViewContext.RouteData.GetRequiredString("action");
            var currentController = htmlHelper.ViewContext.ParentActionViewContext.RouteData.GetRequiredString("controller");

            if (controllerName == currentController && (actionName == currentAction || isRootLevel))
            {
                builder.AddCssClass("active");
            }

            if (string.IsNullOrWhiteSpace(iconClass))
            {
                builder.InnerHtml = htmlHelper.ActionLink(linkText, actionName, controllerName).ToHtmlString();
            }
            else
            {
                builder.InnerHtml =
                    htmlHelper.ActionLink(linkText, actionName, controllerName, iconClass).ToHtmlString();
            }

            return new MvcHtmlString(builder.ToString());
        }

        public static MvcHtmlString ActionLink(this HtmlHelper htmlHelper, string linkText, string actionName,
            string controllerName, string iconClass)
        {
            var span = new TagBuilder("span");
            var icon = new TagBuilder("i");

            span.InnerHtml = linkText;
            icon.AddCssClass(iconClass);

            var anchor = htmlHelper.ActionLink(linkText, actionName, controllerName).ToHtmlString();

            anchor = anchor.Replace(">" + linkText, ">" + icon.ToString() + span.ToString());

            return new MvcHtmlString(anchor.ToString());
        }
    }
}