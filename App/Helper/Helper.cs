﻿//-----------------------------------------------------------------------
// <copyright file="Helper.cs" company="PT. Sangkuriang Internasional">
//     Copyright (c) PT. Sangkuriang Internasional. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.ComponentModel;
using System.IO;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Helpers;
using App.Model;
using Newtonsoft.Json;

namespace App.Helper
{
    /// <summary>
    /// Global helper for website use.
    /// </summary>
    public static class Helper
    {
        /// <summary>
        /// Default Image Extension.
        /// </summary>
        private static readonly string[] DefaultImageExt = { ".jpg", ".png", ".gif", ".jpeg" };
        
        /// <summary>
        /// Truncate string <see cref="input"/> with maximum length to <see cref="maxLength"/>.
        /// </summary>
        /// <param name="input">String to be truncated.</param>
        /// <param name="maxLength">Maximum length of string.</param>
        /// <returns>Truncated string.</returns>
        public static string Truncate(string input, int maxLength)
        {
            return input.Length > maxLength ? string.Format("{0}...", input.Substring(0, maxLength)) : input;
        }

        /// <summary>
        /// Remove html tags from string.
        /// </summary>
        /// <param name="htmlString">Html string to remove tags.</param>
        /// <returns>Removed html tags string.</returns>
        public static string StripHtml(string htmlString)
        {
            if (string.IsNullOrWhiteSpace(htmlString))
            {
                return "";
            }

            return Regex.Replace(htmlString, @"<[^>]*>", String.Empty);
        }

        /// <summary>
        /// Check if path is an Image Path
        /// </summary>
        /// <param name="path">Path to check for image</param>
        /// <param name="imgExt">Image Extension. Default is ".jpg, .png, .gif, .jpeg".</param>
        /// <returns>Boolean</returns>
        public static bool IsImagePath(string path, string[] imgExt = null)
        {
            if (imgExt == null)
            {
                imgExt = DefaultImageExt;
            }

            var extension = Path.GetExtension(path);

            if (extension == null)
            {
                return false;
            }

            var ext = extension.ToLower();

            return Array.IndexOf(imgExt, ext) > -1;
        }

        /// <summary>
        /// Get enum description.
        /// </summary>
        /// <param name="value">Enum value.</param>
        /// <returns>Description of enum value.</returns>
        public static string GetDescription(this Enum value)
        {
            var type = value.GetType();

            var name = Enum.GetName(type, value);
            if (name == null)
            {
                return null;
            }

            var field = type.GetField(name);
            if (field == null)
            {
                return null;
            }

            var attr =
                Attribute.GetCustomAttribute(field,
                    typeof(DescriptionAttribute)) as DescriptionAttribute;

            return attr != null ? attr.Description : null;
        }

        /// <summary>
        /// Show "1 hour ago" or "1 minute ago", instead of seconds.
        /// </summary>
        /// <param name="d">Datetime to convert.</param>
        /// <returns>String of converted datetime. Ex: "1 hour ago" instead of "05/05/2015 23:00"</returns>
        public static string GetPrettyDate(this DateTime d)
        {
            // Get time span elapsed since the date.
            TimeSpan s = DateTime.Now.Subtract(d);

            // Get total number of days elapsed.
            int dayDiff = (int)s.TotalDays;

            // Get total number of seconds elapsed.
            int secDiff = (int)s.TotalSeconds;

            // Don't allow out of range values.
            if (dayDiff < 0 || dayDiff >= 31)
            {
                return null;
            }

            // Handle same-day times.
            if (dayDiff == 0)
            {
                // Less than one minute ago.
                if (secDiff < 60)
                {
                    return "just now";
                }
                // Less than 2 minutes ago.
                if (secDiff < 120)
                {
                    return "1 minute ago";
                }
                // Less than one hour ago.
                if (secDiff < 3600)
                {
                    return string.Format("{0} minutes ago",
                        Math.Floor((double)secDiff / 60));
                }
                // Less than 2 hours ago.
                if (secDiff < 7200)
                {
                    return "1 hour ago";
                }
                // Less than one day ago.
                if (secDiff < 86400)
                {
                    return string.Format("{0} hours ago",
                        Math.Floor((double)secDiff / 3600));
                }
            }
            // Handle previous days.
            if (dayDiff == 1)
            {
                return "yesterday";
            }
            if (dayDiff < 7)
            {
                return string.Format("{0} days ago",
                dayDiff);
            }
            if (dayDiff < 31)
            {
                return string.Format("{0} weeks ago",
                Math.Ceiling((double)dayDiff / 7));
            }
            return null;
        }

        public static string GetQueryStringValue(string key, HttpRequestMessage requestMessage)
        {
            var strings = requestMessage.RequestUri.ParseQueryString().GetValues(key);
            if (strings != null)
            {
                var result = strings[0];
                return result;
            }
            return null;
        }

        /// <summary>
        /// Coordinate converters
        /// </summary>
        public static class Coordinates
        {
            /// <summary>
            /// Convert DMS formated location coordinate to decimal formated location coordinate.
            /// </summary>
            /// <param name="dmsLocation">DMS formated location coordinate</param>
            /// <returns>Decimal formated location coordinate</returns>
            public static DecimalLocation Convert(DmsLocation dmsLocation)
            {
                if (dmsLocation == null)
                {
                    return null;
                }

                return new DecimalLocation
                {
                    Latitude = CalculateDecimal(dmsLocation.Latitude),
                    Longitude = CalculateDecimal(dmsLocation.Longitude)
                };
            }

            /// <summary>
            /// Convert Decimal formated location coordinate to DMS formated location coordinate.
            /// </summary>
            /// <param name="decimalLocation">Decimal formated location coordinate</param>
            /// <returns>DMS formated location coordinate</returns>
            public static DmsLocation Convert(DecimalLocation decimalLocation)
            {
                if (decimalLocation == null)
                {
                    return null;
                }

                return new DmsLocation
                {
                    Latitude = new DmsPoint
                    {
                        Degrees = ExtractDegrees(decimalLocation.Latitude),
                        Minutes = ExtractMinutes(decimalLocation.Latitude),
                        Seconds = ExtractSeconds(decimalLocation.Latitude),
                        Type = PointType.Lat
                    },
                    Longitude = new DmsPoint
                    {
                        Degrees = ExtractDegrees(decimalLocation.Longitude),
                        Minutes = ExtractMinutes(decimalLocation.Longitude),
                        Seconds = ExtractSeconds(decimalLocation.Longitude),
                        Type = PointType.Lon
                    }
                };
            }

            /// <summary>
            /// Convert <see cref="DmsPoint"/> to decimal formated coordinates.
            /// </summary>
            /// <param name="point">DMS point to convert</param>
            /// <returns>Calculated decimal from DMS Point</returns>
            private static decimal CalculateDecimal(DmsPoint point)
            {
                if (point == null)
                {
                    return default(decimal);
                }
                return point.Degrees + (decimal)point.Minutes / 60 + (decimal)point.Seconds / 3600;
            }

            /// <summary>
            /// Get Degrees value from decimal
            /// </summary>
            /// <param name="value">Decimal value to convert to Degrees</param>
            /// <returns>Converted Decimal value to Degrees value</returns>
            private static int ExtractDegrees(decimal value)
            {
                return (int)value;
            }

            /// <summary>
            /// Get Minutes value from decimal
            /// </summary>
            /// <param name="value">Decimal value to convert to Minutes</param>
            /// <returns>Converted Decimal value to Minutes value</returns>
            private static int ExtractMinutes(decimal value)
            {
                value = Math.Abs(value);
                return (int)((value - ExtractDegrees(value)) * 60);
            }

            /// <summary>
            /// Get Seconds value from decimal
            /// </summary>
            /// <param name="value">Decimal value to convert to Seconds</param>
            /// <returns>Converted Decimal value to Seconds value</returns>
            private static int ExtractSeconds(decimal value)
            {
                value = Math.Abs(value);
                decimal minutes = (value - ExtractDegrees(value)) * 60;
                return (int)Math.Round((minutes - ExtractMinutes(value)) * 60);
            }
        }
    }

    public static class ExtensionMethods
    {
        // Deep clone
        public static T DeepClone<T>(this T a)
        {
            var temp = JsonConvert.SerializeObject(a, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                PreserveReferencesHandling = PreserveReferencesHandling.None,
                MaxDepth = 1,
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
            });
            return (T)Json.Decode<T>(temp);
        }
    }
}
