﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Models;
using App.DAL;
using App.Enums;
using Microsoft.AspNet.Identity;

namespace App.Controllers
{
    public class CityEventArgs : EventArgs
    {
        public readonly City After;
        public readonly CrudEventEnumStatus Status;

        public CityEventArgs(City after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class CityController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<CityEventArgs> AfterCall;

        protected virtual void OnCrudOperation(CityEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            City city = db.Cities.Find(id);
            ViewBag.Id = id;
            if (city == null)
            {
                return HttpNotFound();
            }
            return View(city);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(City city)
        {
            if (ModelState.IsValid)
            {
                db.Cities.Add(city);

                db.SaveChanges(User.Identity.GetUserName());
                
                OnCrudOperation(new CityEventArgs(city, CrudEventEnumStatus.Create));

                if (!string.IsNullOrWhiteSpace(Request.QueryString["createNext"]))
                {
                    if (Request.QueryString["createNext"] == "true")
                    {
                        TempData["Created"] = true;
                        TempData["Alert"] = "Berhasil menambahkan City";
                        TempData["AlertClass"] = "success";

                        return RedirectToAction("Create");
                    }
                }

                return RedirectToAction("Details", new { id = city.Id});
            }

            TempData["Alert"] = "Gagal menambahkan City. Harap periksa kembali data yang anda input.";
            TempData["AlertClass"] = "danger";

            return View(city);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            City city = db.Cities.Find(id);
            ViewBag.Id = id;

            if (city == null)
            {
                return HttpNotFound();
            }

            return View(city);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(City city)
        {
            if (ModelState.IsValid)
            {
                db.Entry(city).State = EntityState.Modified;

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new CityEventArgs(city, CrudEventEnumStatus.Update));

                TempData["Alert"] = "Berhasil menyimpan perubahan City";
                TempData["AlertClass"] = "success";

                return RedirectToAction("Details", new { id = city.Id});
            }
            ViewBag.Id = city.Id;

            TempData["Alert"] = "Gagal melakukan perubahan City. Harap periksa kembali data yang anda masukkan.";
            TempData["AlertClass"] = "danger";

            return View(city);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            City city = db.Cities.Find(id);
            ViewBag.Id = id;

            if (city == null)
            {
                return HttpNotFound();
            }
            return View(city);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            City city = db.Cities.Find(id);
            ViewBag.Id = id;
            db.Cities.Remove(city);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new CityEventArgs(city, CrudEventEnumStatus.Delete));
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
