﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Models;
using App.DAL;
using App.Enums;
using Microsoft.AspNet.Identity;

namespace App.Controllers
{
    public class BranchOfficeEventArgs : EventArgs
    {
        public readonly BranchOffice After;
        public readonly CrudEventEnumStatus Status;

        public BranchOfficeEventArgs(BranchOffice after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class BranchOfficeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<BranchOfficeEventArgs> AfterCall;

        protected virtual void OnCrudOperation(BranchOfficeEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BranchOffice branchOffice = db.BranchOffices.Find(id);
            ViewBag.Id = id;
            if (branchOffice == null)
            {
                return HttpNotFound();
            }
            return View(branchOffice);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BranchOffice branchOffice)
        {
            if (ModelState.IsValid)
            {
                db.BranchOffices.Add(branchOffice);

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new BranchOfficeEventArgs(branchOffice, CrudEventEnumStatus.Create));

                if (!string.IsNullOrWhiteSpace(Request.QueryString["createNext"]))
                {
                    if (Request.QueryString["createNext"] == "true")
                    {
                        TempData["Created"] = true;
                        TempData["Alert"] = "Berhasil menambahkan BranchOffice";
                        TempData["AlertClass"] = "success";

                        return RedirectToAction("Create");
                    }
                }

                return RedirectToAction("Details", new { id = branchOffice.Id});
            }

            TempData["Alert"] = "Gagal menambahkan BranchOffice. Harap periksa kembali data yang anda input.";
            TempData["AlertClass"] = "danger";

            return View(branchOffice);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BranchOffice branchOffice = db.BranchOffices.Find(id);
            ViewBag.Id = id;

            if (branchOffice == null)
            {
                return HttpNotFound();
            }

            return View(branchOffice);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BranchOffice branchOffice)
        {
            if (ModelState.IsValid)
            {
                db.Entry(branchOffice).State = EntityState.Modified;

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new BranchOfficeEventArgs(branchOffice, CrudEventEnumStatus.Update));

                TempData["Alert"] = "Berhasil menyimpan perubahan BranchOffice";
                TempData["AlertClass"] = "success";

                return RedirectToAction("Details", new { id = branchOffice.Id});
            }
            ViewBag.Id = branchOffice.Id;

            TempData["Alert"] = "Gagal melakukan perubahan BranchOffice. Harap periksa kembali data yang anda masukkan.";
            TempData["AlertClass"] = "danger";

            return View(branchOffice);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            BranchOffice branchOffice = db.BranchOffices.Find(id);
            ViewBag.Id = id;

            if (branchOffice == null)
            {
                return HttpNotFound();
            }
            return View(branchOffice);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BranchOffice branchOffice = db.BranchOffices.Find(id);
            ViewBag.Id = id;
            db.BranchOffices.Remove(branchOffice);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new BranchOfficeEventArgs(branchOffice, CrudEventEnumStatus.Delete));
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
