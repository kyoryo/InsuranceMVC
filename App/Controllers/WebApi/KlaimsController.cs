﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Kendo.DynamicLinq;
using Newtonsoft.Json;
using Domain.Models;
using App.DAL;
using App.Enums;
using App.Helper;
using Domain.Models.ViewModels;
using Microsoft.AspNet.Identity;

namespace App.Controllers.WebApi
{
    public class KlaimsControllerEventArgs : EventArgs
    {
        public readonly Klaim After;
        public readonly CrudEventEnumStatus Status;

        public KlaimsControllerEventArgs(Klaim after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class KlaimsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<KlaimsControllerEventArgs> AfterCall;

        protected virtual void OnCrudOperation(KlaimsControllerEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        // GET: api/Klaims
        public DataSourceResult GetKlaims(HttpRequestMessage requestMessage)
        {
            DataSourceRequest request =
                JsonConvert.DeserializeObject<DataSourceRequest>(requestMessage.RequestUri.ParseQueryString().GetValues("q")[0]);

            if (request.Filter != null && !request.Filter.Filters.Any())
            {
                request.Filter = null;
            }
            else
            {
                request.Filter.ToCustomExpression();
            }

            var result = db.Klaims.Include(x => x.KerjaKlaim).Include(x => x.PemegangPolis).AsQueryable();

            return result
                .OrderBy(x => x.Id)
                .Project()
                .To<KlaimViewModel>()
                .ToDataSourceResult(request.Take, request.Skip, request.Sort, request.Filter);
        }

        // GET: api/Klaims/5
        [ResponseType(typeof(KlaimViewModel))]
        public IHttpActionResult GetKlaim(int id)
        {
            Klaim klaim = db.Klaims.Find(id);
            if (klaim == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<Klaim, KlaimViewModel>(klaim));
        }

        // PUT: api/Klaims/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutKlaim(int id, KlaimViewModel klaim)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != klaim.Id)
            {
                return BadRequest();
            }

            var tempklaim = klaim.ToEntity();

            db.Entry(tempklaim).State = EntityState.Modified;

            try
            {
                db.SaveChanges(User.Identity.GetUserName());
                OnCrudOperation(new KlaimsControllerEventArgs(tempklaim, CrudEventEnumStatus.Update));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!KlaimExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Klaims
        [ResponseType(typeof(KlaimViewModel))]
        public IHttpActionResult PostKlaim(KlaimViewModel klaim)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var tempklaim = klaim.ToEntity();

            db.Klaims.Add(tempklaim);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new KlaimsControllerEventArgs(tempklaim, CrudEventEnumStatus.Create));

            return CreatedAtRoute("DefaultApi", new { id = klaim.Id }, 
                    Mapper.Map<Klaim, KlaimViewModel>(tempklaim));
        }

        // DELETE: api/Klaims/5
        [ResponseType(typeof(KlaimViewModel))]
        public IHttpActionResult DeleteKlaim(int id)
        {
            Klaim klaim = db.Klaims.Find(id);
            if (klaim == null)
            {
                return NotFound();
            }

            db.Klaims.Remove(klaim);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new KlaimsControllerEventArgs(klaim, CrudEventEnumStatus.Delete));

            return Ok(Mapper.Map<Klaim, KlaimViewModel>(klaim));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool KlaimExists(int id)
        {
            return db.Klaims.Count(e => e.Id == id) > 0;
        }
    }
}