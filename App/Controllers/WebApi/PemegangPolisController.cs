﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Kendo.DynamicLinq;
using Newtonsoft.Json;
using Domain.Models;
using App.DAL;
using App.Enums;
using App.Helper;
using Domain.Models.ViewModels;
using Microsoft.AspNet.Identity;

namespace App.Controllers.WebApi
{
    public class PemegangPolisControllerEventArgs : EventArgs
    {
        public readonly PemegangPolis After;
        public readonly CrudEventEnumStatus Status;

        public PemegangPolisControllerEventArgs(PemegangPolis after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class PemegangPolisController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<PemegangPolisControllerEventArgs> AfterCall;

        protected virtual void OnCrudOperation(PemegangPolisControllerEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        // GET: api/PemegangPolis
        public DataSourceResult GetPemegangPolis(HttpRequestMessage requestMessage)
        {
            DataSourceRequest request =
                JsonConvert.DeserializeObject<DataSourceRequest>(requestMessage.RequestUri.ParseQueryString().GetValues("q")[0]);

            if (request.Filter != null && !request.Filter.Filters.Any())
            {
                request.Filter = null;
            }
            else
            {
                request.Filter.ToCustomExpression();
            }

            var result = db.PemegangPolis.Include(x => x.Owner).AsQueryable();

            return result
                .OrderBy(x => x.Id)
                .Project()
                .To<PemegangPolisViewModel>()
                .ToDataSourceResult(request.Take, request.Skip, request.Sort, request.Filter);
        }

        // GET: api/PemegangPolis/5
        [ResponseType(typeof(PemegangPolisViewModel))]
        public IHttpActionResult GetPemegangPolis(int id)
        {
            PemegangPolis pemegangPolis = db.PemegangPolis.Find(id);
            if (pemegangPolis == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<PemegangPolis, PemegangPolisViewModel>(pemegangPolis));
        }

        // PUT: api/PemegangPolis/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPemegangPolis(int id, PemegangPolisViewModel pemegangPolis)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pemegangPolis.Id)
            {
                return BadRequest();
            }

            var temppemegangPolis = pemegangPolis.ToEntity();

            db.Entry(temppemegangPolis).State = EntityState.Modified;

            try
            {
                db.SaveChanges(User.Identity.GetUserName());
                OnCrudOperation(new PemegangPolisControllerEventArgs(temppemegangPolis, CrudEventEnumStatus.Update));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PemegangPolisExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PemegangPolis
        [ResponseType(typeof(PemegangPolisViewModel))]
        public IHttpActionResult PostPemegangPolis(PemegangPolisViewModel pemegangPolis)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var temppemegangPolis = pemegangPolis.ToEntity();

            db.PemegangPolis.Add(temppemegangPolis);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new PemegangPolisControllerEventArgs(temppemegangPolis, CrudEventEnumStatus.Create));

            return CreatedAtRoute("DefaultApi", new { id = pemegangPolis.Id }, 
                    Mapper.Map<PemegangPolis, PemegangPolisViewModel>(temppemegangPolis));
        }

        // DELETE: api/PemegangPolis/5
        [ResponseType(typeof(PemegangPolisViewModel))]
        public IHttpActionResult DeletePemegangPolis(int id)
        {
            PemegangPolis pemegangPolis = db.PemegangPolis.Find(id);
            if (pemegangPolis == null)
            {
                return NotFound();
            }

            db.PemegangPolis.Remove(pemegangPolis);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new PemegangPolisControllerEventArgs(pemegangPolis, CrudEventEnumStatus.Delete));

            return Ok(Mapper.Map<PemegangPolis, PemegangPolisViewModel>(pemegangPolis));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PemegangPolisExists(int id)
        {
            return db.PemegangPolis.Count(e => e.Id == id) > 0;
        }
    }
}