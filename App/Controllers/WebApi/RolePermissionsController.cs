﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Kendo.DynamicLinq;
using Newtonsoft.Json;
using Domain.Infrastructure;
using App.DAL;
using App.Enums;
using App.Helper;
using Domain.Models.ViewModels;
using Microsoft.AspNet.Identity;

namespace App.Controllers.WebApi
{
    public class RolePermissionsControllerEventArgs : EventArgs
    {
        public readonly RolePermission After;
        public readonly CrudEventEnumStatus Status;

        public RolePermissionsControllerEventArgs(RolePermission after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class RolePermissionsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<RolePermissionsControllerEventArgs> AfterCall;

        protected virtual void OnCrudOperation(RolePermissionsControllerEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        // GET: api/RolePermissions
        public DataSourceResult GetRolePermissions(HttpRequestMessage requestMessage)
        {
            DataSourceRequest request =
                JsonConvert.DeserializeObject<DataSourceRequest>(requestMessage.RequestUri.ParseQueryString().GetValues("q")[0]);

            if (request.Filter != null && !request.Filter.Filters.Any())
            {
                request.Filter = null;
            }
            else
            {
                request.Filter.ToCustomExpression();
            }

            var result = db.RolePermissions.Include(x => x.Permission).Include(x => x.Role).AsQueryable();

            return result
                .OrderBy(x => x.Id)
                .Project()
                .To<RolePermissionViewModel>()
                .ToDataSourceResult(request.Take, request.Skip, request.Sort, request.Filter);
        }

        // GET: api/RolePermissions/5
        [ResponseType(typeof(RolePermissionViewModel))]
        public IHttpActionResult GetRolePermission(Guid id)
        {
            RolePermission rolePermission = db.RolePermissions.Find(id);
            if (rolePermission == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<RolePermission, RolePermissionViewModel>(rolePermission));
        }

        // PUT: api/RolePermissions/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutRolePermission(Guid id, RolePermissionViewModel rolePermission)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != rolePermission.Id)
            {
                return BadRequest();
            }

            var temprolePermission = rolePermission.ToEntity();

            db.Entry(temprolePermission).State = EntityState.Modified;

            try
            {
                db.SaveChanges(User.Identity.GetUserName());
                OnCrudOperation(new RolePermissionsControllerEventArgs(temprolePermission, CrudEventEnumStatus.Update));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RolePermissionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/RolePermissions
        [ResponseType(typeof(RolePermissionViewModel))]
        public IHttpActionResult PostRolePermission([FromBody] IList<RolePermissionViewModel> rolePermissions)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (rolePermissions.Count == 0)
            {
                return BadRequest();
            }

            var ids = new List<RolePermissionViewModel>();

            foreach (var rolePermission in rolePermissions)
            {
                var temp = rolePermission.ToEntity();

                db.RolePermissions.Add(temp);
                db.SaveChanges(User.Identity.GetUserName());

                ids.Add(Mapper.Map<RolePermission, RolePermissionViewModel>(temp));

                OnCrudOperation(new RolePermissionsControllerEventArgs(temp, CrudEventEnumStatus.Create));
            }

            return Ok(ids);
        }

        // DELETE: api/RolePermissions/5
        [ResponseType(typeof(RolePermissionViewModel))]
        public IHttpActionResult DeleteRolePermission(Guid id)
        {
            RolePermission rolePermission = db.RolePermissions.Find(id);
            if (rolePermission == null)
            {
                return NotFound();
            }

            db.RolePermissions.Remove(rolePermission);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new RolePermissionsControllerEventArgs(rolePermission, CrudEventEnumStatus.Delete));

            return Ok(Mapper.Map<RolePermission, RolePermissionViewModel>(rolePermission));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RolePermissionExists(Guid id)
        {
            return db.RolePermissions.Count(e => e.Id == id) > 0;
        }
    }
}