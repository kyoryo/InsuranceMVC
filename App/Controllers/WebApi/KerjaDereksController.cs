﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Kendo.DynamicLinq;
using Newtonsoft.Json;
using Domain.Models;
using App.DAL;
using App.Enums;
using App.Helper;
using Domain.Models.ViewModels;
using Microsoft.AspNet.Identity;

namespace App.Controllers.WebApi
{
    public class KerjaDereksControllerEventArgs : EventArgs
    {
        public readonly KerjaDerek After;
        public readonly CrudEventEnumStatus Status;

        public KerjaDereksControllerEventArgs(KerjaDerek after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class KerjaDereksController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<KerjaDereksControllerEventArgs> AfterCall;

        protected virtual void OnCrudOperation(KerjaDereksControllerEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        // GET: api/KerjaDereks
        public DataSourceResult GetKerjaDereks(HttpRequestMessage requestMessage)
        {
            DataSourceRequest request =
                JsonConvert.DeserializeObject<DataSourceRequest>(requestMessage.RequestUri.ParseQueryString().GetValues("q")[0]);

            if (request.Filter != null && !request.Filter.Filters.Any())
            {
                request.Filter = null;
            }
            else
            {
                request.Filter.ToCustomExpression();
            }

            var result = db.KerjaDereks.AsQueryable();

            return result
                .OrderBy(x => x.Id)
                .Project()
                .To<KerjaDerekViewModel>()
                .ToDataSourceResult(request.Take, request.Skip, request.Sort, request.Filter);
        }

        // GET: api/KerjaDereks/5
        [ResponseType(typeof(KerjaDerekViewModel))]
        public IHttpActionResult GetKerjaDerek(int id)
        {
            KerjaDerek kerjaDerek = db.KerjaDereks.Find(id);
            if (kerjaDerek == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<KerjaDerek, KerjaDerekViewModel>(kerjaDerek));
        }

        // PUT: api/KerjaDereks/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutKerjaDerek(int id, KerjaDerekViewModel kerjaDerek)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != kerjaDerek.Id)
            {
                return BadRequest();
            }

            var tempkerjaDerek = kerjaDerek.ToEntity();

            db.Entry(tempkerjaDerek).State = EntityState.Modified;

            try
            {
                db.SaveChanges(User.Identity.GetUserName());
                OnCrudOperation(new KerjaDereksControllerEventArgs(tempkerjaDerek, CrudEventEnumStatus.Update));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!KerjaDerekExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/KerjaDereks
        [ResponseType(typeof(KerjaDerekViewModel))]
        public IHttpActionResult PostKerjaDerek(KerjaDerekViewModel kerjaDerek)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var tempkerjaDerek = kerjaDerek.ToEntity();

            db.KerjaDereks.Add(tempkerjaDerek);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new KerjaDereksControllerEventArgs(tempkerjaDerek, CrudEventEnumStatus.Create));

            return CreatedAtRoute("DefaultApi", new { id = kerjaDerek.Id }, 
                    Mapper.Map<KerjaDerek, KerjaDerekViewModel>(tempkerjaDerek));
        }

        // DELETE: api/KerjaDereks/5
        [ResponseType(typeof(KerjaDerekViewModel))]
        public IHttpActionResult DeleteKerjaDerek(int id)
        {
            KerjaDerek kerjaDerek = db.KerjaDereks.Find(id);
            if (kerjaDerek == null)
            {
                return NotFound();
            }

            db.KerjaDereks.Remove(kerjaDerek);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new KerjaDereksControllerEventArgs(kerjaDerek, CrudEventEnumStatus.Delete));

            return Ok(Mapper.Map<KerjaDerek, KerjaDerekViewModel>(kerjaDerek));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool KerjaDerekExists(int id)
        {
            return db.KerjaDereks.Count(e => e.Id == id) > 0;
        }
    }
}