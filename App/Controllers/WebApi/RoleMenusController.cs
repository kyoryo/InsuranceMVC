﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Kendo.DynamicLinq;
using Newtonsoft.Json;
using Domain.Infrastructure;
using App.DAL;
using App.Enums;
using App.Helper;
using Domain.Models.ViewModels;
using Microsoft.AspNet.Identity;

namespace App.Controllers.WebApi
{
    public class RoleMenusControllerEventArgs : EventArgs
    {
        public readonly RoleMenu After;
        public readonly CrudEventEnumStatus Status;

        public RoleMenusControllerEventArgs(RoleMenu after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class RoleMenusController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<RoleMenusControllerEventArgs> AfterCall;

        protected virtual void OnCrudOperation(RoleMenusControllerEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        // GET: api/RoleMenus
        public DataSourceResult GetRoleMenus(HttpRequestMessage requestMessage)
        {
            DataSourceRequest request =
                JsonConvert.DeserializeObject<DataSourceRequest>(requestMessage.RequestUri.ParseQueryString().GetValues("q")[0]);

            if (request.Filter != null && !request.Filter.Filters.Any())
            {
                request.Filter = null;
            }
            else
            {
                request.Filter.ToCustomExpression();
            }

            var result = db.RoleMenus.Include(x => x.Menu).Include(x => x.Role).AsQueryable();

            return result
                .OrderBy(x => x.Id)
                .Project()
                .To<RoleMenuViewModel>()
                .ToDataSourceResult(request.Take, request.Skip, request.Sort, request.Filter);
        }

        // GET: api/RoleMenus/5
        [ResponseType(typeof(RoleMenuViewModel))]
        public IHttpActionResult GetRoleMenu(Guid id)
        {
            RoleMenu roleMenu = db.RoleMenus.Find(id);
            if (roleMenu == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<RoleMenu, RoleMenuViewModel>(roleMenu));
        }

        // PUT: api/RoleMenus/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutRoleMenu(Guid id, RoleMenuViewModel roleMenu)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != roleMenu.Id)
            {
                return BadRequest();
            }

            var temproleMenu = roleMenu.ToEntity();

            db.Entry(temproleMenu).State = EntityState.Modified;

            try
            {
                db.SaveChanges(User.Identity.GetUserName());
                OnCrudOperation(new RoleMenusControllerEventArgs(temproleMenu, CrudEventEnumStatus.Update));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RoleMenuExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/RoleMenus
        [ResponseType(typeof(RoleMenuViewModel))]
        public IHttpActionResult PostRoleMenu([FromBody] IList<RoleMenuViewModel> roleMenus)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (roleMenus.Count == 0)
            {
                return BadRequest();
            }

            var ids = new List<RoleMenuViewModel>();

            foreach (var roleMenu in roleMenus)
            {
                var temp = roleMenu.ToEntity();

                db.RoleMenus.Add(temp);
                db.SaveChanges(User.Identity.GetUserName());

                ids.Add(Mapper.Map<RoleMenu, RoleMenuViewModel>(temp));

                OnCrudOperation(new RoleMenusControllerEventArgs(temp, CrudEventEnumStatus.Create));
            }

            return Ok(ids);
        }

        // DELETE: api/RoleMenus/5
        [ResponseType(typeof(RoleMenuViewModel))]
        public IHttpActionResult DeleteRoleMenu(Guid id)
        {
            RoleMenu roleMenu = db.RoleMenus.Find(id);
            if (roleMenu == null)
            {
                return NotFound();
            }

            db.RoleMenus.Remove(roleMenu);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new RoleMenusControllerEventArgs(roleMenu, CrudEventEnumStatus.Delete));

            return Ok(Mapper.Map<RoleMenu, RoleMenuViewModel>(roleMenu));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RoleMenuExists(Guid id)
        {
            return db.RoleMenus.Count(e => e.Id == id) > 0;
        }
    }
}