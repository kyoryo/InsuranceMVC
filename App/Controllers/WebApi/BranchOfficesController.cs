﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Kendo.DynamicLinq;
using Newtonsoft.Json;
using Domain.Models;
using App.DAL;
using App.Enums;
using App.Helper;
using Domain.Models.ViewModels;
using Microsoft.AspNet.Identity;

namespace App.Controllers.WebApi
{
    public class BranchOfficesControllerEventArgs : EventArgs
    {
        public readonly BranchOffice After;
        public readonly CrudEventEnumStatus Status;

        public BranchOfficesControllerEventArgs(BranchOffice after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class BranchOfficesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<BranchOfficesControllerEventArgs> AfterCall;

        protected virtual void OnCrudOperation(BranchOfficesControllerEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        // GET: api/BranchOffices
        public DataSourceResult GetBranchOffices(HttpRequestMessage requestMessage)
        {
            DataSourceRequest request =
                JsonConvert.DeserializeObject<DataSourceRequest>(requestMessage.RequestUri.ParseQueryString().GetValues("q")[0]);

            if (request.Filter != null && !request.Filter.Filters.Any())
            {
                request.Filter = null;
            }
            else
            {
                request.Filter.ToCustomExpression();
            }

            var result = db.BranchOffices.Include(x => x.City).AsQueryable();

            return result
                .OrderBy(x => x.Id)
                .Project()
                .To<BranchOfficeViewModel>()
                .ToDataSourceResult(request.Take, request.Skip, request.Sort, request.Filter);
        }

        // GET: api/BranchOffices/5
        [ResponseType(typeof(BranchOfficeViewModel))]
        public IHttpActionResult GetBranchOffice(int id)
        {
            BranchOffice branchOffice = db.BranchOffices.Find(id);
            if (branchOffice == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<BranchOffice, BranchOfficeViewModel>(branchOffice));
        }

        // PUT: api/BranchOffices/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutBranchOffice(int id, BranchOfficeViewModel branchOffice)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != branchOffice.Id)
            {
                return BadRequest();
            }

            var tempbranchOffice = branchOffice.ToEntity();

            db.Entry(tempbranchOffice).State = EntityState.Modified;

            try
            {
                db.SaveChanges(User.Identity.GetUserName());
                OnCrudOperation(new BranchOfficesControllerEventArgs(tempbranchOffice, CrudEventEnumStatus.Update));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BranchOfficeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/BranchOffices
        [ResponseType(typeof(BranchOfficeViewModel))]
        public IHttpActionResult PostBranchOffice(BranchOfficeViewModel branchOffice)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var tempbranchOffice = branchOffice.ToEntity();

            db.BranchOffices.Add(tempbranchOffice);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new BranchOfficesControllerEventArgs(tempbranchOffice, CrudEventEnumStatus.Create));

            return CreatedAtRoute("DefaultApi", new { id = branchOffice.Id }, 
                    Mapper.Map<BranchOffice, BranchOfficeViewModel>(tempbranchOffice));
        }

        // DELETE: api/BranchOffices/5
        [ResponseType(typeof(BranchOfficeViewModel))]
        public IHttpActionResult DeleteBranchOffice(int id)
        {
            BranchOffice branchOffice = db.BranchOffices.Find(id);
            if (branchOffice == null)
            {
                return NotFound();
            }

            db.BranchOffices.Remove(branchOffice);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new BranchOfficesControllerEventArgs(branchOffice, CrudEventEnumStatus.Delete));

            return Ok(Mapper.Map<BranchOffice, BranchOfficeViewModel>(branchOffice));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BranchOfficeExists(int id)
        {
            return db.BranchOffices.Count(e => e.Id == id) > 0;
        }
    }
}