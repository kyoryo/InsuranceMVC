﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Kendo.DynamicLinq;
using Newtonsoft.Json;
using Domain.Models;
using App.DAL;
using App.Enums;
using App.Helper;
using Domain.Models.ViewModels;
using Microsoft.AspNet.Identity;

namespace App.Controllers.WebApi
{
    public class KerjaKlaimsControllerEventArgs : EventArgs
    {
        public readonly KerjaKlaim After;
        public readonly CrudEventEnumStatus Status;

        public KerjaKlaimsControllerEventArgs(KerjaKlaim after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class KerjaKlaimsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<KerjaKlaimsControllerEventArgs> AfterCall;

        protected virtual void OnCrudOperation(KerjaKlaimsControllerEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        // GET: api/KerjaKlaims
        public DataSourceResult GetKerjaKlaims(HttpRequestMessage requestMessage)
        {
            DataSourceRequest request =
                JsonConvert.DeserializeObject<DataSourceRequest>(requestMessage.RequestUri.ParseQueryString().GetValues("q")[0]);

            if (request.Filter != null && !request.Filter.Filters.Any())
            {
                request.Filter = null;
            }
            else
            {
                request.Filter.ToCustomExpression();
            }

            var result = db.KerjaKlaims.AsQueryable();

            return result
                .OrderBy(x => x.Id)
                .Project()
                .To<KerjaKlaimViewModel>()
                .ToDataSourceResult(request.Take, request.Skip, request.Sort, request.Filter);
        }

        // GET: api/KerjaKlaims/5
        [ResponseType(typeof(KerjaKlaimViewModel))]
        public IHttpActionResult GetKerjaKlaim(int id)
        {
            KerjaKlaim kerjaKlaim = db.KerjaKlaims.Find(id);
            if (kerjaKlaim == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<KerjaKlaim, KerjaKlaimViewModel>(kerjaKlaim));
        }

        // PUT: api/KerjaKlaims/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutKerjaKlaim(int id, KerjaKlaimViewModel kerjaKlaim)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != kerjaKlaim.Id)
            {
                return BadRequest();
            }

            var tempkerjaKlaim = kerjaKlaim.ToEntity();

            db.Entry(tempkerjaKlaim).State = EntityState.Modified;

            try
            {
                db.SaveChanges(User.Identity.GetUserName());
                OnCrudOperation(new KerjaKlaimsControllerEventArgs(tempkerjaKlaim, CrudEventEnumStatus.Update));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!KerjaKlaimExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/KerjaKlaims
        [ResponseType(typeof(KerjaKlaimViewModel))]
        public IHttpActionResult PostKerjaKlaim(KerjaKlaimViewModel kerjaKlaim)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var tempkerjaKlaim = kerjaKlaim.ToEntity();

            db.KerjaKlaims.Add(tempkerjaKlaim);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new KerjaKlaimsControllerEventArgs(tempkerjaKlaim, CrudEventEnumStatus.Create));

            return CreatedAtRoute("DefaultApi", new { id = kerjaKlaim.Id }, 
                    Mapper.Map<KerjaKlaim, KerjaKlaimViewModel>(tempkerjaKlaim));
        }

        // DELETE: api/KerjaKlaims/5
        [ResponseType(typeof(KerjaKlaimViewModel))]
        public IHttpActionResult DeleteKerjaKlaim(int id)
        {
            KerjaKlaim kerjaKlaim = db.KerjaKlaims.Find(id);
            if (kerjaKlaim == null)
            {
                return NotFound();
            }

            db.KerjaKlaims.Remove(kerjaKlaim);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new KerjaKlaimsControllerEventArgs(kerjaKlaim, CrudEventEnumStatus.Delete));

            return Ok(Mapper.Map<KerjaKlaim, KerjaKlaimViewModel>(kerjaKlaim));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool KerjaKlaimExists(int id)
        {
            return db.KerjaKlaims.Count(e => e.Id == id) > 0;
        }
    }
}