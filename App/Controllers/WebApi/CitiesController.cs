﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Kendo.DynamicLinq;
using Newtonsoft.Json;
using Domain.Models;
using App.DAL;
using App.Enums;
using App.Helper;
using Domain.Models.ViewModels;
using Microsoft.AspNet.Identity;

namespace App.Controllers.WebApi
{
    public class CitiesControllerEventArgs : EventArgs
    {
        public readonly City After;
        public readonly CrudEventEnumStatus Status;

        public CitiesControllerEventArgs(City after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class CitiesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<CitiesControllerEventArgs> AfterCall;

        protected virtual void OnCrudOperation(CitiesControllerEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        // GET: api/Cities
        public DataSourceResult GetCities(HttpRequestMessage requestMessage)
        {
            DataSourceRequest request =
                JsonConvert.DeserializeObject<DataSourceRequest>(requestMessage.RequestUri.ParseQueryString().GetValues("q")[0]);

            if (request.Filter != null && !request.Filter.Filters.Any())
            {
                request.Filter = null;
            }
            else
            {
                request.Filter.ToCustomExpression();
            }

            var result = db.Cities.AsQueryable();

            return result
                .OrderBy(x => x.Id)
                .Project()
                .To<CityViewModel>()
                .ToDataSourceResult(request.Take, request.Skip, request.Sort, request.Filter);
        }

        // GET: api/Cities/5
        [ResponseType(typeof(CityViewModel))]
        public IHttpActionResult GetCity(int id)
        {
            City city = db.Cities.Find(id);
            if (city == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<City, CityViewModel>(city));
        }

        // PUT: api/Cities/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCity(int id, CityViewModel city)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != city.Id)
            {
                return BadRequest();
            }

            var tempcity = city.ToEntity();

            db.Entry(tempcity).State = EntityState.Modified;

            try
            {
                db.SaveChanges(User.Identity.GetUserName());
                OnCrudOperation(new CitiesControllerEventArgs(tempcity, CrudEventEnumStatus.Update));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Cities
        [ResponseType(typeof(CityViewModel))]
        public IHttpActionResult PostCity(CityViewModel city)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var tempcity = city.ToEntity();

            db.Cities.Add(tempcity);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new CitiesControllerEventArgs(tempcity, CrudEventEnumStatus.Create));

            return CreatedAtRoute("DefaultApi", new { id = city.Id }, 
                    Mapper.Map<City, CityViewModel>(tempcity));
        }

        // DELETE: api/Cities/5
        [ResponseType(typeof(CityViewModel))]
        public IHttpActionResult DeleteCity(int id)
        {
            City city = db.Cities.Find(id);
            if (city == null)
            {
                return NotFound();
            }

            db.Cities.Remove(city);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new CitiesControllerEventArgs(city, CrudEventEnumStatus.Delete));

            return Ok(Mapper.Map<City, CityViewModel>(city));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CityExists(int id)
        {
            return db.Cities.Count(e => e.Id == id) > 0;
        }
    }
}