﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using App.DAL;
using App.Enums;
using App.Helper;
using App.Infrastructure;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Domain.Infrastructure;
using Domain.Models.ViewModels;
using Humanizer;
using Kendo.DynamicLinq;
using Newtonsoft.Json;

namespace App.Controllers.WebApi
{
    public class UserManagementsControllerEventArgs : EventArgs, IEventArgs<ApplicationUser>
    {
        public CrudEventEnumStatus Status { get; set; }
        public ApplicationUser After { get; set; }

        public UserManagementsControllerEventArgs(ApplicationUser after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class UserManagementsController : BaseApiController
    {
        private readonly ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<UserManagementsControllerEventArgs> AfterCall;

        protected virtual void OnCrudOperation(UserManagementsControllerEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        // GET: api/Candidates
        public DataSourceResult GetUsers(HttpRequestMessage requestMessage)
        {
            DataSourceRequest request =
                JsonConvert.DeserializeObject<DataSourceRequest>(Helper.Helper.GetQueryStringValue("q", requestMessage));

            request.Filter.ToCustomExpression();

            var result = db.Users.Include(x => x.UserProfile).AsQueryable();

            string activeStatus = UserStatus.Active.Humanize();
            string notActoveStatus = UserStatus.NotActive.Humanize();

            return result
                .OrderBy(x => x.Id)
                .Project()
                .To<BaseUser>()
                .ToDataSourceResult(request.Take, request.Skip, request.Sort, request.Filter);
        }

        // DELETE: api/Rooms/5
        [ResponseType(typeof (BaseUser))]
        public IHttpActionResult DeleteUser(string id)
        {
            var user = db.Users.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            db.Users.Remove(user);
            db.SaveChanges();
            OnCrudOperation(new UserManagementsControllerEventArgs(user, CrudEventEnumStatus.Delete));

            return Ok(Mapper.Map<ApplicationUser, BaseUser>(user));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserExists(string id)
        {
            return db.Users.Count(e => e.Id == id) > 0;
        }
    }
}