﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Kendo.DynamicLinq;
using Newtonsoft.Json;
using Domain.Models;
using App.DAL;
using App.Enums;
using App.Helper;
using Domain.Models.ViewModels;
using Microsoft.AspNet.Identity;

namespace App.Controllers.WebApi
{
    public class ProductsControllerEventArgs : EventArgs
    {
        public readonly Product After;
        public readonly CrudEventEnumStatus Status;

        public ProductsControllerEventArgs(Product after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class ProductsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<ProductsControllerEventArgs> AfterCall;

        protected virtual void OnCrudOperation(ProductsControllerEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        // GET: api/Products
        public DataSourceResult GetProducts(HttpRequestMessage requestMessage)
        {
            DataSourceRequest request =
                JsonConvert.DeserializeObject<DataSourceRequest>(requestMessage.RequestUri.ParseQueryString().GetValues("q")[0]);

            if (request.Filter != null && !request.Filter.Filters.Any())
            {
                request.Filter = null;
            }
            else
            {
                request.Filter.ToCustomExpression();
            }

            var result = db.Products.Include(x => x.Category).AsQueryable();

            return result
                .OrderBy(x => x.Id)
                .Project()
                .To<ProductViewModel>()
                .ToDataSourceResult(request.Take, request.Skip, request.Sort, request.Filter);
        }

        // GET: api/Products/5
        [ResponseType(typeof(ProductViewModel))]
        public IHttpActionResult GetProduct(Guid id)
        {
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<Product, ProductViewModel>(product));
        }

        // PUT: api/Products/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutProduct(Guid id, ProductViewModel product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != product.Id)
            {
                return BadRequest();
            }

            var tempproduct = product.ToEntity();

            db.Entry(tempproduct).State = EntityState.Modified;

            try
            {
                db.SaveChanges(User.Identity.GetUserName());
                OnCrudOperation(new ProductsControllerEventArgs(tempproduct, CrudEventEnumStatus.Update));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Products
        [ResponseType(typeof(ProductViewModel))]
        public IHttpActionResult PostProduct(ProductViewModel product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var tempproduct = product.ToEntity();

            db.Products.Add(tempproduct);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new ProductsControllerEventArgs(tempproduct, CrudEventEnumStatus.Create));

            return CreatedAtRoute("DefaultApi", new { id = product.Id }, 
                    Mapper.Map<Product, ProductViewModel>(tempproduct));
        }

        // DELETE: api/Products/5
        [ResponseType(typeof(ProductViewModel))]
        public IHttpActionResult DeleteProduct(Guid id)
        {
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return NotFound();
            }

            db.Products.Remove(product);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new ProductsControllerEventArgs(product, CrudEventEnumStatus.Delete));

            return Ok(Mapper.Map<Product, ProductViewModel>(product));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProductExists(Guid id)
        {
            return db.Products.Count(e => e.Id == id) > 0;
        }
    }
}