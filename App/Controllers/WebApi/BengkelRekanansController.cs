﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Kendo.DynamicLinq;
using Newtonsoft.Json;
using Domain.Models;
using App.DAL;
using App.Enums;
using App.Helper;
using Domain.Models.ViewModels;
using Microsoft.AspNet.Identity;

namespace App.Controllers.WebApi
{
    public class BengkelRekanansControllerEventArgs : EventArgs
    {
        public readonly BengkelRekanan After;
        public readonly CrudEventEnumStatus Status;

        public BengkelRekanansControllerEventArgs(BengkelRekanan after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class BengkelRekanansController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<BengkelRekanansControllerEventArgs> AfterCall;

        protected virtual void OnCrudOperation(BengkelRekanansControllerEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        // GET: api/BengkelRekanans
        public DataSourceResult GetBengkelRekanans(HttpRequestMessage requestMessage)
        {
            DataSourceRequest request =
                JsonConvert.DeserializeObject<DataSourceRequest>(requestMessage.RequestUri.ParseQueryString().GetValues("q")[0]);

            if (request.Filter != null && !request.Filter.Filters.Any())
            {
                request.Filter = null;
            }
            else
            {
                request.Filter.ToCustomExpression();
            }

            var result = db.BengkelRekanans.Include(x => x.City).AsQueryable();

            return result
                .OrderBy(x => x.Id)
                .Project()
                .To<BengkelRekananViewModel>()
                .ToDataSourceResult(request.Take, request.Skip, request.Sort, request.Filter);
        }

        // GET: api/BengkelRekanans/5
        [ResponseType(typeof(BengkelRekananViewModel))]
        public IHttpActionResult GetBengkelRekanan(int id)
        {
            BengkelRekanan bengkelRekanan = db.BengkelRekanans.Find(id);
            if (bengkelRekanan == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<BengkelRekanan, BengkelRekananViewModel>(bengkelRekanan));
        }

        // PUT: api/BengkelRekanans/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutBengkelRekanan(int id, BengkelRekananViewModel bengkelRekanan)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != bengkelRekanan.Id)
            {
                return BadRequest();
            }

            var tempbengkelRekanan = bengkelRekanan.ToEntity();

            db.Entry(tempbengkelRekanan).State = EntityState.Modified;

            try
            {
                db.SaveChanges(User.Identity.GetUserName());
                OnCrudOperation(new BengkelRekanansControllerEventArgs(tempbengkelRekanan, CrudEventEnumStatus.Update));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BengkelRekananExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/BengkelRekanans
        [ResponseType(typeof(BengkelRekananViewModel))]
        public IHttpActionResult PostBengkelRekanan(BengkelRekananViewModel bengkelRekanan)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var tempbengkelRekanan = bengkelRekanan.ToEntity();

            db.BengkelRekanans.Add(tempbengkelRekanan);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new BengkelRekanansControllerEventArgs(tempbengkelRekanan, CrudEventEnumStatus.Create));

            return CreatedAtRoute("DefaultApi", new { id = bengkelRekanan.Id }, 
                    Mapper.Map<BengkelRekanan, BengkelRekananViewModel>(tempbengkelRekanan));
        }

        // DELETE: api/BengkelRekanans/5
        [ResponseType(typeof(BengkelRekananViewModel))]
        public IHttpActionResult DeleteBengkelRekanan(int id)
        {
            BengkelRekanan bengkelRekanan = db.BengkelRekanans.Find(id);
            if (bengkelRekanan == null)
            {
                return NotFound();
            }

            db.BengkelRekanans.Remove(bengkelRekanan);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new BengkelRekanansControllerEventArgs(bengkelRekanan, CrudEventEnumStatus.Delete));

            return Ok(Mapper.Map<BengkelRekanan, BengkelRekananViewModel>(bengkelRekanan));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BengkelRekananExists(int id)
        {
            return db.BengkelRekanans.Count(e => e.Id == id) > 0;
        }
    }
}