﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Kendo.DynamicLinq;
using Newtonsoft.Json;
using Domain.Models;
using App.DAL;
using App.Enums;
using App.Helper;
using Domain.Models.ViewModels;
using Microsoft.AspNet.Identity;

namespace App.Controllers.WebApi
{
    public class DereksControllerEventArgs : EventArgs
    {
        public readonly Derek After;
        public readonly CrudEventEnumStatus Status;

        public DereksControllerEventArgs(Derek after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class DereksController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<DereksControllerEventArgs> AfterCall;

        protected virtual void OnCrudOperation(DereksControllerEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        // GET: api/Dereks
        public DataSourceResult GetDereks(HttpRequestMessage requestMessage)
        {
            DataSourceRequest request =
                JsonConvert.DeserializeObject<DataSourceRequest>(requestMessage.RequestUri.ParseQueryString().GetValues("q")[0]);

            if (request.Filter != null && !request.Filter.Filters.Any())
            {
                request.Filter = null;
            }
            else
            {
                request.Filter.ToCustomExpression();
            }

            var result = db.Dereks.Include(x => x.City).Include(x => x.KerjaDerek).Include(x => x.PemegangPolis).AsQueryable();

            return result
                .OrderBy(x => x.Id)
                .Project()
                .To<DerekViewModel>()
                .ToDataSourceResult(request.Take, request.Skip, request.Sort, request.Filter);
        }

        // GET: api/Dereks/5
        [ResponseType(typeof(DerekViewModel))]
        public IHttpActionResult GetDerek(int id)
        {
            Derek derek = db.Dereks.Find(id);
            if (derek == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<Derek, DerekViewModel>(derek));
        }

        // PUT: api/Dereks/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDerek(int id, DerekViewModel derek)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != derek.Id)
            {
                return BadRequest();
            }

            var tempderek = derek.ToEntity();

            db.Entry(tempderek).State = EntityState.Modified;

            try
            {
                db.SaveChanges(User.Identity.GetUserName());
                OnCrudOperation(new DereksControllerEventArgs(tempderek, CrudEventEnumStatus.Update));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DerekExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Dereks
        [ResponseType(typeof(DerekViewModel))]
        public IHttpActionResult PostDerek(DerekViewModel derek)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var tempderek = derek.ToEntity();

            db.Dereks.Add(tempderek);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new DereksControllerEventArgs(tempderek, CrudEventEnumStatus.Create));

            return CreatedAtRoute("DefaultApi", new { id = derek.Id }, 
                    Mapper.Map<Derek, DerekViewModel>(tempderek));
        }

        // DELETE: api/Dereks/5
        [ResponseType(typeof(DerekViewModel))]
        public IHttpActionResult DeleteDerek(int id)
        {
            Derek derek = db.Dereks.Find(id);
            if (derek == null)
            {
                return NotFound();
            }

            db.Dereks.Remove(derek);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new DereksControllerEventArgs(derek, CrudEventEnumStatus.Delete));

            return Ok(Mapper.Map<Derek, DerekViewModel>(derek));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DerekExists(int id)
        {
            return db.Dereks.Count(e => e.Id == id) > 0;
        }
    }
}