﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Kendo.DynamicLinq;
using Newtonsoft.Json;
using Domain.Infrastructure;
using App.DAL;
using App.Enums;
using App.Helper;
using Domain.Models.ViewModels;
using Microsoft.AspNet.Identity;

namespace App.Controllers.WebApi
{
    public class PermissionsControllerEventArgs : EventArgs
    {
        public readonly Permission After;
        public readonly CrudEventEnumStatus Status;

        public PermissionsControllerEventArgs(Permission after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class PermissionsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<PermissionsControllerEventArgs> AfterCall;

        protected virtual void OnCrudOperation(PermissionsControllerEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        // GET: api/Permissions
        public DataSourceResult GetPermissions(HttpRequestMessage requestMessage)
        {
            DataSourceRequest request =
                JsonConvert.DeserializeObject<DataSourceRequest>(requestMessage.RequestUri.ParseQueryString().GetValues("q")[0]);

            if (request.Filter != null && !request.Filter.Filters.Any())
            {
                request.Filter = null;
            }
            else
            {
                request.Filter.ToCustomExpression();
            }

            var result = db.Permissions.Include(x => x.RolePermissions).AsQueryable();

            if (!string.IsNullOrWhiteSpace(Helper.Helper.GetQueryStringValue("role", requestMessage)))
            {
                var roleId = Helper.Helper.GetQueryStringValue("role", requestMessage);
                var registeredRoles = db.RolePermissions.Where(x => x.RoleId.ToString() == roleId);

                result = result.Where(x => !registeredRoles.Any(r => r.PermissionId == x.Id));
            }

            return result
                .OrderBy(x => x.Name)
                .Project()
                .To<PermissionViewModel>()
                .ToDataSourceResult(request.Take, request.Skip, request.Sort, request.Filter);
        }

        // GET: api/Permissions/5
        [ResponseType(typeof(PermissionViewModel))]
        public IHttpActionResult GetPermission(Guid id)
        {
            Permission permission = db.Permissions.Find(id);
            if (permission == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<Permission, PermissionViewModel>(permission));
        }

        // PUT: api/Permissions/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPermission(Guid id, PermissionViewModel permission)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != permission.Id)
            {
                return BadRequest();
            }

            var temppermission = permission.ToEntity();

            db.Entry(temppermission).State = EntityState.Modified;

            try
            {
                db.SaveChanges(User.Identity.GetUserName());
                OnCrudOperation(new PermissionsControllerEventArgs(temppermission, CrudEventEnumStatus.Update));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PermissionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Permissions
        [ResponseType(typeof(PermissionViewModel))]
        public IHttpActionResult PostPermission(PermissionViewModel permission)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var temppermission = permission.ToEntity();

            db.Permissions.Add(temppermission);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new PermissionsControllerEventArgs(temppermission, CrudEventEnumStatus.Create));

            return CreatedAtRoute("DefaultApi", new { id = permission.Id }, 
                    Mapper.Map<Permission, PermissionViewModel>(temppermission));
        }

        // DELETE: api/Permissions/5
        [ResponseType(typeof(PermissionViewModel))]
        public IHttpActionResult DeletePermission(Guid id)
        {
            Permission permission = db.Permissions.Find(id);
            if (permission == null)
            {
                return NotFound();
            }

            db.Permissions.Remove(permission);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new PermissionsControllerEventArgs(permission, CrudEventEnumStatus.Delete));

            return Ok(Mapper.Map<Permission, PermissionViewModel>(permission));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PermissionExists(Guid id)
        {
            return db.Permissions.Count(e => e.Id == id) > 0;
        }
    }
}