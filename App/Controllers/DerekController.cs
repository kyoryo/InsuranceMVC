﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Models;
using App.DAL;
using App.Enums;
using Microsoft.AspNet.Identity;

namespace App.Controllers
{
    public class DerekEventArgs : EventArgs
    {
        public readonly Derek After;
        public readonly CrudEventEnumStatus Status;

        public DerekEventArgs(Derek after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class DerekController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<DerekEventArgs> AfterCall;

        protected virtual void OnCrudOperation(DerekEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Derek derek = db.Dereks.Find(id);
            ViewBag.Id = id;
            if (derek == null)
            {
                return HttpNotFound();
            }
            return View(derek);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Derek derek)
        {
            if (ModelState.IsValid)
            {
                db.Dereks.Add(derek);

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new DerekEventArgs(derek, CrudEventEnumStatus.Create));

                if (!string.IsNullOrWhiteSpace(Request.QueryString["createNext"]))
                {
                    if (Request.QueryString["createNext"] == "true")
                    {
                        TempData["Created"] = true;
                        TempData["Alert"] = "Berhasil menambahkan Derek";
                        TempData["AlertClass"] = "success";

                        return RedirectToAction("Create");
                    }
                }

                return RedirectToAction("Details", new { id = derek.Id});
            }

            TempData["Alert"] = "Gagal menambahkan Derek. Harap periksa kembali data yang anda input.";
            TempData["AlertClass"] = "danger";

            return View(derek);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Derek derek = db.Dereks.Find(id);
            ViewBag.Id = id;

            if (derek == null)
            {
                return HttpNotFound();
            }

            return View(derek);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Derek derek)
        {
            if (ModelState.IsValid)
            {
                db.Entry(derek).State = EntityState.Modified;

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new DerekEventArgs(derek, CrudEventEnumStatus.Update));

                TempData["Alert"] = "Berhasil menyimpan perubahan Derek";
                TempData["AlertClass"] = "success";

                return RedirectToAction("Details", new { id = derek.Id});
            }
            ViewBag.Id = derek.Id;

            TempData["Alert"] = "Gagal melakukan perubahan Derek. Harap periksa kembali data yang anda masukkan.";
            TempData["AlertClass"] = "danger";

            return View(derek);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Derek derek = db.Dereks.Find(id);
            ViewBag.Id = id;

            if (derek == null)
            {
                return HttpNotFound();
            }
            return View(derek);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Derek derek = db.Dereks.Find(id);
            ViewBag.Id = id;
            db.Dereks.Remove(derek);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new DerekEventArgs(derek, CrudEventEnumStatus.Delete));
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
