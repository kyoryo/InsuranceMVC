﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Infrastructure;
using App.DAL;
using App.Enums;
using App.Infrastructure;
using Microsoft.AspNet.Identity;

namespace App.Controllers
{
    [PermissionAuthorization("administrator_only")]
    public class RolePermissionEventArgs : EventArgs
    {
        public readonly RolePermission After;
        public readonly CrudEventEnumStatus Status;

        public RolePermissionEventArgs(RolePermission after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class RolePermissionController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<RolePermissionEventArgs> AfterCall;

        protected virtual void OnCrudOperation(RolePermissionEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RolePermission rolePermission = db.RolePermissions.Find(id);
            ViewBag.Id = id;
            if (rolePermission == null)
            {
                return HttpNotFound();
            }
            return View(rolePermission);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RolePermission rolePermission)
        {
            if (ModelState.IsValid)
            {
                rolePermission.Id = Guid.NewGuid();
                db.RolePermissions.Add(rolePermission);

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new RolePermissionEventArgs(rolePermission, CrudEventEnumStatus.Create));

                if (!string.IsNullOrWhiteSpace(Request.QueryString["createNext"]))
                {
                    if (Request.QueryString["createNext"] == "true")
                    {
                        TempData["Created"] = true;
                        TempData["Alert"] = "Berhasil menambahkan RolePermission";
                        TempData["AlertClass"] = "success";

                        return RedirectToAction("Create");
                    }
                }

                return RedirectToAction("Details", new { id = rolePermission.Id});
            }

            TempData["Alert"] = "Gagal menambahkan RolePermission. Harap periksa kembali data yang anda input.";
            TempData["AlertClass"] = "danger";

            return View(rolePermission);
        }

        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RolePermission rolePermission = db.RolePermissions.Find(id);
            ViewBag.Id = id;

            if (rolePermission == null)
            {
                return HttpNotFound();
            }

            return View(rolePermission);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(RolePermission rolePermission)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rolePermission).State = EntityState.Modified;

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new RolePermissionEventArgs(rolePermission, CrudEventEnumStatus.Update));

                TempData["Alert"] = "Berhasil menyimpan perubahan RolePermission";
                TempData["AlertClass"] = "success";

                return RedirectToAction("Details", new { id = rolePermission.Id});
            }
            ViewBag.Id = rolePermission.Id;

            TempData["Alert"] = "Gagal melakukan perubahan RolePermission. Harap periksa kembali data yang anda masukkan.";
            TempData["AlertClass"] = "danger";

            return View(rolePermission);
        }

        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            RolePermission rolePermission = db.RolePermissions.Find(id);
            ViewBag.Id = id;

            if (rolePermission == null)
            {
                return HttpNotFound();
            }
            return View(rolePermission);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            RolePermission rolePermission = db.RolePermissions.Find(id);
            ViewBag.Id = id;
            db.RolePermissions.Remove(rolePermission);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new RolePermissionEventArgs(rolePermission, CrudEventEnumStatus.Delete));
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
