﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Models;
using App.DAL;
using App.Enums;
using Microsoft.AspNet.Identity;

namespace App.Controllers
{
    public class PemegangPolisEventArgs : EventArgs
    {
        public readonly PemegangPolis After;
        public readonly CrudEventEnumStatus Status;

        public PemegangPolisEventArgs(PemegangPolis after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class PemegangPolisController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<PemegangPolisEventArgs> AfterCall;

        protected virtual void OnCrudOperation(PemegangPolisEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PemegangPolis pemegangPolis = db.PemegangPolis.Find(id);
            ViewBag.Id = id;
            if (pemegangPolis == null)
            {
                return HttpNotFound();
            }
            return View(pemegangPolis);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PemegangPolis pemegangPolis)
        {
            /// NOTES: IF (admin) then show user list from ASP.USER
            /// IF USER then cannot create OR 
            /// in an alternative case, User List become his/her own ID (shown as USERNAME/EMAIL)
            /// ---------not done
            /// may be applied in EDIT too
            /// ---------
            /// for a time being this actionresult will use his/her own ID (unshown)
            if (ModelState.IsValid)
            {
                pemegangPolis.OwnerId = User.Identity.GetUserId();

                db.PemegangPolis.Add(pemegangPolis);
                
                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new PemegangPolisEventArgs(pemegangPolis, CrudEventEnumStatus.Create));

                if (!string.IsNullOrWhiteSpace(Request.QueryString["createNext"]))
                {
                    if (Request.QueryString["createNext"] == "true")
                    {
                        TempData["Created"] = true;
                        TempData["Alert"] = "Berhasil menambahkan PemegangPolis";
                        TempData["AlertClass"] = "success";

                        return RedirectToAction("Create");
                    }
                }

                return RedirectToAction("Details", new { id = pemegangPolis.Id});
            }

            TempData["Alert"] = "Gagal menambahkan PemegangPolis. Harap periksa kembali data yang anda input.";
            TempData["AlertClass"] = "danger";

            return View(pemegangPolis);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PemegangPolis pemegangPolis = db.PemegangPolis.Find(id);
            ViewBag.Id = id;

            if (pemegangPolis == null)
            {
                return HttpNotFound();
            }

            return View(pemegangPolis);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PemegangPolis pemegangPolis)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pemegangPolis).State = EntityState.Modified;

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new PemegangPolisEventArgs(pemegangPolis, CrudEventEnumStatus.Update));

                TempData["Alert"] = "Berhasil menyimpan perubahan PemegangPolis";
                TempData["AlertClass"] = "success";

                return RedirectToAction("Details", new { id = pemegangPolis.Id});
            }
            ViewBag.Id = pemegangPolis.Id;

            TempData["Alert"] = "Gagal melakukan perubahan PemegangPolis. Harap periksa kembali data yang anda masukkan.";
            TempData["AlertClass"] = "danger";

            return View(pemegangPolis);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            PemegangPolis pemegangPolis = db.PemegangPolis.Find(id);
            ViewBag.Id = id;

            if (pemegangPolis == null)
            {
                return HttpNotFound();
            }
            return View(pemegangPolis);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PemegangPolis pemegangPolis = db.PemegangPolis.Find(id);
            ViewBag.Id = id;
            db.PemegangPolis.Remove(pemegangPolis);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new PemegangPolisEventArgs(pemegangPolis, CrudEventEnumStatus.Delete));
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
