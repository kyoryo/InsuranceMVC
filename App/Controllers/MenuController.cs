﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Infrastructure;
using App.DAL;
using App.Enums;
using App.Infrastructure;
using Microsoft.AspNet.Identity;

namespace App.Controllers
{
    public class MenuEventArgs : EventArgs
    {
        public readonly Menu After;
        public readonly CrudEventEnumStatus Status;

        public MenuEventArgs(Menu after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class MenuController : BaseUserController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<MenuEventArgs> AfterCall;

        protected virtual void OnCrudOperation(MenuEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        [PermissionAuthorization("read_master_data")]
        public ActionResult Index()
        {
            return View();
        }

        [PermissionAuthorization("read_master_data")]
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Menu menu = db.Menus.Find(id);
            ViewBag.Id = id;
            if (menu == null)
            {
                return HttpNotFound();
            }
            return View(menu);
        }

        [PermissionAuthorization("crud_master_data")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [PermissionAuthorization("crud_master_data")]
        public ActionResult Create(Menu menu)
        {
            if (ModelState.IsValid)
            {
                menu.Id = Guid.NewGuid();
                db.Menus.Add(menu);

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new MenuEventArgs(menu, CrudEventEnumStatus.Create));

                if (!string.IsNullOrWhiteSpace(Request.QueryString["createNext"]))
                {
                    if (Request.QueryString["createNext"] == "true")
                    {
                        TempData["Created"] = true;
                        TempData["Alert"] = "Berhasil menambahkan Menu";
                        TempData["AlertClass"] = "success";

                        return RedirectToAction("Create");
                    }
                }

                return RedirectToAction("Details", new { id = menu.Id});
            }

            TempData["Alert"] = "Gagal menambahkan Menu. Harap periksa kembali data yang anda input.";
            TempData["AlertClass"] = "danger";

            return View(menu);
        }

        [PermissionAuthorization("crud_master_data")]
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Menu menu = db.Menus.Find(id);
            ViewBag.Id = id;

            if (menu == null)
            {
                return HttpNotFound();
            }

            return View(menu);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [PermissionAuthorization("crud_master_data")]
        public ActionResult Edit(Menu menu)
        {
            if (ModelState.IsValid)
            {
                db.Entry(menu).State = EntityState.Modified;

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new MenuEventArgs(menu, CrudEventEnumStatus.Update));

                TempData["Alert"] = "Berhasil menyimpan perubahan Menu";
                TempData["AlertClass"] = "success";

                return RedirectToAction("Details", new { id = menu.Id});
            }
            ViewBag.Id = menu.Id;

            TempData["Alert"] = "Gagal melakukan perubahan Menu. Harap periksa kembali data yang anda masukkan.";
            TempData["AlertClass"] = "danger";

            return View(menu);
        }

        [ChildActionOnly]
        [AllowAnonymous]
        public ActionResult RenderMenu()
        {
            var userId = User.Identity.GetUserId();
            var roles = AppUserManager.GetRoles(userId);

            var menus = db.Menus.AsQueryable();

            foreach (var role in roles)
            {
                var roleId = AppRoleManager.FindByName(role);
                menus = menus.Where(x => x.RoleMenus.Any(r => r.RoleId == roleId.Id));
            }

            menus = menus.OrderBy(x => x.SortOrder);

            return PartialView("_RenderMenu", menus.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
