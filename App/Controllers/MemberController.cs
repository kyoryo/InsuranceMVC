﻿using System;
using System.Data.Entity;
using System.Linq;
using App.DAL;
using System.IO;
using App.Enums;
using System.Web.Mvc;
using Domain.Infrastructure;
using Microsoft.AspNet.Identity;
using AutoMapper;
using Domain.Models.ViewModels;

namespace App.Controllers
{
    [Authorize]
    public class MemberController : BaseUserController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public static event EventHandler<UserProfileEventArgs> AfterCall;
        private static readonly string[] _validImageTypes = new string[] { ".png", ".jpg", ".jpeg", ".gif" };
        protected virtual void OnCrudOperation(UserProfileEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }
        [ActionName("Index")]
        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Profile()
        {
            var user = this.AppUserManager.FindById(User.Identity.GetUserId());
            //var userProf = Mapper.Map<ApplicationUser, BaseUser>(user);
            ViewBag.Role = this.AppRoleManager.FindById(user.Roles.First().RoleId);

            return View(user);
        }

        public ActionResult Detail(string id)
        {
            int Id;
            ApplicationUser user = null;
            user = int.TryParse(id, out Id) ? db.Users.First(x => x.UserProfile.Id == Id) : this.AppUserManager.FindById(id);
            var userProf = Mapper.Map<ApplicationUser, BaseUser>(user);
            return View(userProf);
        }

        public ActionResult Edit()
        {
            var user = this.AppUserManager.FindById(User.Identity.GetUserId());

            if (user == null)
            {
                return HttpNotFound();
            }

            var editUserAccount = Mapper.Map<ApplicationUser, BaseUser>(user);

            ViewBag.Path = null;
            return View(editUserAccount);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BaseUser baseuser)
        {

            if (ModelState.IsValid)
            {
                var user = this.AppUserManager.FindById(baseuser.Id);

                if (user == null)
                {
                    return HttpNotFound();
                }

                user.PhoneNumber = baseuser.Telephone;
                user.UserName = baseuser.UserName;
                user.Email = baseuser.Email;

                var result = this.AppUserManager.Update(user);

                if (result.Succeeded)
                {
                    var userProfile = db.UserProfiles.Find(user.UserProfile.Id);

                    userProfile.Name = baseuser.Name;
                    userProfile.Address = baseuser.Address;
                    userProfile.Birthdate = baseuser.BirthDate;
                    userProfile.Birthplace = baseuser.BirthPlace;

                    if (baseuser.Photo != null && baseuser.Photo != userProfile.Photo)
                    {
                        var rootPath = string.Format("/Content/uploads/{0}/", baseuser.Id.ToString());
                        var directory = Server.MapPath(rootPath);

                        if (!Directory.Exists(directory))
                        {
                            Directory.CreateDirectory(directory);
                        }

                        var path = rootPath + Path.GetFileName(baseuser.Photo);
                        var ext = Path.GetExtension(baseuser.Photo);
                        if (!_validImageTypes.Contains(ext))
                        {
                            return View(baseuser);
                        }
                        string[] oldphoto = Directory.GetFiles(Server.MapPath(rootPath));
                        foreach (var old in oldphoto)
                        {
                            System.IO.File.Delete(old);
                        }
                        System.IO.File.Move(Server.MapPath(baseuser.Photo), Server.MapPath(path));

                        userProfile.Photo = path;
                    }

                    db.Entry(userProfile).State = EntityState.Modified;

                    db.SaveChanges();                    

                    TempData["message"] = string.Format("Your profile detail successfully updated");

                    OnCrudOperation(new UserProfileEventArgs(userProfile, CrudEventEnumStatus.Update));

                    return RedirectToAction("Edit");
                }
                AddErrors(result);                
            }
                return View(baseuser);
        }

        public ActionResult Manage()
        {
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        public ActionResult ForgotPassword()
        {
            return View();
        }

        public ActionResult Confirmed()
        {
            return View();
        }

        public ActionResult ResetPassword(string userId = "", string token = "")
        {
            if (string.IsNullOrWhiteSpace(userId) || string.IsNullOrWhiteSpace(token))
            {
                RedirectToAction("ForgotPassword");
            }

            ViewBag.Id = userId;
            ViewBag.Token = token.Replace(" ", "+");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Logout()
        {
            if (Response.Cookies["auth"] != null)
            {
                Response.Cookies["auth"].Expires = DateTime.Now.AddDays(-1);

                Session.Abandon();
            }
            return RedirectToAction("Index");
        }

        private void AddErrors(IdentityResult result)
        {
            TempData["message"] = "";
            foreach (var error in result.Errors)
            {
                TempData["message"] += error;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}