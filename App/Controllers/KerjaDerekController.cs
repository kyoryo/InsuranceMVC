﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Models;
using App.DAL;
using App.Enums;
using Microsoft.AspNet.Identity;

namespace App.Controllers
{
    public class KerjaDerekEventArgs : EventArgs
    {
        public readonly KerjaDerek After;
        public readonly CrudEventEnumStatus Status;

        public KerjaDerekEventArgs(KerjaDerek after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class KerjaDerekController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<KerjaDerekEventArgs> AfterCall;

        protected virtual void OnCrudOperation(KerjaDerekEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KerjaDerek kerjaDerek = db.KerjaDereks.Find(id);
            ViewBag.Id = id;
            if (kerjaDerek == null)
            {
                return HttpNotFound();
            }
            return View(kerjaDerek);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(KerjaDerek kerjaDerek)
        {
            if (ModelState.IsValid)
            {
                db.KerjaDereks.Add(kerjaDerek);

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new KerjaDerekEventArgs(kerjaDerek, CrudEventEnumStatus.Create));

                if (!string.IsNullOrWhiteSpace(Request.QueryString["createNext"]))
                {
                    if (Request.QueryString["createNext"] == "true")
                    {
                        TempData["Created"] = true;
                        TempData["Alert"] = "Berhasil menambahkan KerjaDerek";
                        TempData["AlertClass"] = "success";

                        return RedirectToAction("Create");
                    }
                }

                return RedirectToAction("Details", new { id = kerjaDerek.Id});
            }

            TempData["Alert"] = "Gagal menambahkan KerjaDerek. Harap periksa kembali data yang anda input.";
            TempData["AlertClass"] = "danger";

            return View(kerjaDerek);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KerjaDerek kerjaDerek = db.KerjaDereks.Find(id);
            ViewBag.Id = id;

            if (kerjaDerek == null)
            {
                return HttpNotFound();
            }

            return View(kerjaDerek);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(KerjaDerek kerjaDerek)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kerjaDerek).State = EntityState.Modified;

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new KerjaDerekEventArgs(kerjaDerek, CrudEventEnumStatus.Update));

                TempData["Alert"] = "Berhasil menyimpan perubahan KerjaDerek";
                TempData["AlertClass"] = "success";

                return RedirectToAction("Details", new { id = kerjaDerek.Id});
            }
            ViewBag.Id = kerjaDerek.Id;

            TempData["Alert"] = "Gagal melakukan perubahan KerjaDerek. Harap periksa kembali data yang anda masukkan.";
            TempData["AlertClass"] = "danger";

            return View(kerjaDerek);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            KerjaDerek kerjaDerek = db.KerjaDereks.Find(id);
            ViewBag.Id = id;

            if (kerjaDerek == null)
            {
                return HttpNotFound();
            }
            return View(kerjaDerek);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            KerjaDerek kerjaDerek = db.KerjaDereks.Find(id);
            ViewBag.Id = id;
            db.KerjaDereks.Remove(kerjaDerek);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new KerjaDerekEventArgs(kerjaDerek, CrudEventEnumStatus.Delete));
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
