﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Models;
using App.DAL;
using App.Enums;
using Microsoft.AspNet.Identity;

namespace App.Controllers
{
    public class KlaimEventArgs : EventArgs
    {
        public readonly Klaim After;
        public readonly CrudEventEnumStatus Status;

        public KlaimEventArgs(Klaim after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class KlaimController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<KlaimEventArgs> AfterCall;

        protected virtual void OnCrudOperation(KlaimEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Klaim klaim = db.Klaims.Find(id);
            ViewBag.Id = id;
            if (klaim == null)
            {
                return HttpNotFound();
            }
            return View(klaim);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Klaim klaim)
        {
            if (ModelState.IsValid)
            {
                db.Klaims.Add(klaim);

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new KlaimEventArgs(klaim, CrudEventEnumStatus.Create));

                if (!string.IsNullOrWhiteSpace(Request.QueryString["createNext"]))
                {
                    if (Request.QueryString["createNext"] == "true")
                    {
                        TempData["Created"] = true;
                        TempData["Alert"] = "Berhasil menambahkan Klaim";
                        TempData["AlertClass"] = "success";

                        return RedirectToAction("Create");
                    }
                }

                return RedirectToAction("Details", new { id = klaim.Id});
            }

            TempData["Alert"] = "Gagal menambahkan Klaim. Harap periksa kembali data yang anda input.";
            TempData["AlertClass"] = "danger";

            return View(klaim);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Klaim klaim = db.Klaims.Find(id);
            ViewBag.Id = id;

            if (klaim == null)
            {
                return HttpNotFound();
            }

            return View(klaim);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Klaim klaim)
        {
            if (ModelState.IsValid)
            {
                db.Entry(klaim).State = EntityState.Modified;

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new KlaimEventArgs(klaim, CrudEventEnumStatus.Update));

                TempData["Alert"] = "Berhasil menyimpan perubahan Klaim";
                TempData["AlertClass"] = "success";

                return RedirectToAction("Details", new { id = klaim.Id});
            }
            ViewBag.Id = klaim.Id;

            TempData["Alert"] = "Gagal melakukan perubahan Klaim. Harap periksa kembali data yang anda masukkan.";
            TempData["AlertClass"] = "danger";

            return View(klaim);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Klaim klaim = db.Klaims.Find(id);
            ViewBag.Id = id;

            if (klaim == null)
            {
                return HttpNotFound();
            }
            return View(klaim);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Klaim klaim = db.Klaims.Find(id);
            ViewBag.Id = id;
            db.Klaims.Remove(klaim);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new KlaimEventArgs(klaim, CrudEventEnumStatus.Delete));
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
