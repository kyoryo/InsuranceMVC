﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Models;
using App.DAL;
using App.Enums;
using Microsoft.AspNet.Identity;

namespace App.Controllers
{
    public class BengkelRekananEventArgs : EventArgs
    {
        public readonly BengkelRekanan After;
        public readonly CrudEventEnumStatus Status;

        public BengkelRekananEventArgs(BengkelRekanan after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class BengkelRekananController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<BengkelRekananEventArgs> AfterCall;

        protected virtual void OnCrudOperation(BengkelRekananEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BengkelRekanan bengkelRekanan = db.BengkelRekanans.Find(id);
            ViewBag.Id = id;
            if (bengkelRekanan == null)
            {
                return HttpNotFound();
            }
            return View(bengkelRekanan);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BengkelRekanan bengkelRekanan)
        {
            if (ModelState.IsValid)
            {
                db.BengkelRekanans.Add(bengkelRekanan);

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new BengkelRekananEventArgs(bengkelRekanan, CrudEventEnumStatus.Create));

                if (!string.IsNullOrWhiteSpace(Request.QueryString["createNext"]))
                {
                    if (Request.QueryString["createNext"] == "true")
                    {
                        TempData["Created"] = true;
                        TempData["Alert"] = "Berhasil menambahkan BengkelRekanan";
                        TempData["AlertClass"] = "success";

                        return RedirectToAction("Create");
                    }
                }

                return RedirectToAction("Details", new { id = bengkelRekanan.Id});
            }

            TempData["Alert"] = "Gagal menambahkan BengkelRekanan. Harap periksa kembali data yang anda input.";
            TempData["AlertClass"] = "danger";

            return View(bengkelRekanan);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BengkelRekanan bengkelRekanan = db.BengkelRekanans.Find(id);
            ViewBag.Id = id;

            if (bengkelRekanan == null)
            {
                return HttpNotFound();
            }

            return View(bengkelRekanan);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BengkelRekanan bengkelRekanan)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bengkelRekanan).State = EntityState.Modified;

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new BengkelRekananEventArgs(bengkelRekanan, CrudEventEnumStatus.Update));

                TempData["Alert"] = "Berhasil menyimpan perubahan BengkelRekanan";
                TempData["AlertClass"] = "success";

                return RedirectToAction("Details", new { id = bengkelRekanan.Id});
            }
            ViewBag.Id = bengkelRekanan.Id;

            TempData["Alert"] = "Gagal melakukan perubahan BengkelRekanan. Harap periksa kembali data yang anda masukkan.";
            TempData["AlertClass"] = "danger";

            return View(bengkelRekanan);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            BengkelRekanan bengkelRekanan = db.BengkelRekanans.Find(id);
            ViewBag.Id = id;

            if (bengkelRekanan == null)
            {
                return HttpNotFound();
            }
            return View(bengkelRekanan);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BengkelRekanan bengkelRekanan = db.BengkelRekanans.Find(id);
            ViewBag.Id = id;
            db.BengkelRekanans.Remove(bengkelRekanan);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new BengkelRekananEventArgs(bengkelRekanan, CrudEventEnumStatus.Delete));
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
