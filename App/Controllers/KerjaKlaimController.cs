﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Models;
using App.DAL;
using App.Enums;
using Microsoft.AspNet.Identity;

namespace App.Controllers
{
    public class KerjaKlaimEventArgs : EventArgs
    {
        public readonly KerjaKlaim After;
        public readonly CrudEventEnumStatus Status;

        public KerjaKlaimEventArgs(KerjaKlaim after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class KerjaKlaimController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<KerjaKlaimEventArgs> AfterCall;

        protected virtual void OnCrudOperation(KerjaKlaimEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KerjaKlaim kerjaKlaim = db.KerjaKlaims.Find(id);
            ViewBag.Id = id;
            if (kerjaKlaim == null)
            {
                return HttpNotFound();
            }
            return View(kerjaKlaim);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(KerjaKlaim kerjaKlaim)
        {
            if (ModelState.IsValid)
            {
                db.KerjaKlaims.Add(kerjaKlaim);

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new KerjaKlaimEventArgs(kerjaKlaim, CrudEventEnumStatus.Create));

                if (!string.IsNullOrWhiteSpace(Request.QueryString["createNext"]))
                {
                    if (Request.QueryString["createNext"] == "true")
                    {
                        TempData["Created"] = true;
                        TempData["Alert"] = "Berhasil menambahkan KerjaKlaim";
                        TempData["AlertClass"] = "success";

                        return RedirectToAction("Create");
                    }
                }

                return RedirectToAction("Details", new { id = kerjaKlaim.Id});
            }

            TempData["Alert"] = "Gagal menambahkan KerjaKlaim. Harap periksa kembali data yang anda input.";
            TempData["AlertClass"] = "danger";

            return View(kerjaKlaim);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KerjaKlaim kerjaKlaim = db.KerjaKlaims.Find(id);
            ViewBag.Id = id;

            if (kerjaKlaim == null)
            {
                return HttpNotFound();
            }

            return View(kerjaKlaim);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(KerjaKlaim kerjaKlaim)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kerjaKlaim).State = EntityState.Modified;

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new KerjaKlaimEventArgs(kerjaKlaim, CrudEventEnumStatus.Update));

                TempData["Alert"] = "Berhasil menyimpan perubahan KerjaKlaim";
                TempData["AlertClass"] = "success";

                return RedirectToAction("Details", new { id = kerjaKlaim.Id});
            }
            ViewBag.Id = kerjaKlaim.Id;

            TempData["Alert"] = "Gagal melakukan perubahan KerjaKlaim. Harap periksa kembali data yang anda masukkan.";
            TempData["AlertClass"] = "danger";

            return View(kerjaKlaim);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            KerjaKlaim kerjaKlaim = db.KerjaKlaims.Find(id);
            ViewBag.Id = id;

            if (kerjaKlaim == null)
            {
                return HttpNotFound();
            }
            return View(kerjaKlaim);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            KerjaKlaim kerjaKlaim = db.KerjaKlaims.Find(id);
            ViewBag.Id = id;
            db.KerjaKlaims.Remove(kerjaKlaim);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new KerjaKlaimEventArgs(kerjaKlaim, CrudEventEnumStatus.Delete));
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
