﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using App.DAL;
using System.IO;
using System.Linq.Expressions;
using System.Web;
using System.Web.Hosting;
using System.Web.Security;
using App.Enums;
using App.Infrastructure;
using Domain.Infrastructure;
using Domain.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;

namespace App.Controllers
{
    public class UserProfileEventArgs : EventArgs, IEventArgs<UserProfile>
    {
        public CrudEventEnumStatus Status { get; set; }
        public UserProfile After { get; set; }

        public UserProfileEventArgs(UserProfile after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    [PermissionAuthorization("administrator_only")]
    public class UserManagementController : BaseUserController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<UserProfileEventArgs> AfterCall;

        private static readonly string[] _validImageTypes = new string[] { ".png", ".jpg", ".jpeg", ".gif" };

        protected virtual void OnCrudOperation(UserProfileEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        // GET: UserManagement
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            ViewBag.Id = id;

            ViewBag.Role = this.AppRoleManager.FindById(user.Roles.First().RoleId);

            return View(user);
        }

        public ActionResult Create()
        {
            //CreateUserBindingModel userModel = new CreateUserBindingModel
            //{
            //    RoleName = db.Roles.OrderBy(r => r.Name).Select(rr =>
            //    new SelectListItem { Value = rr.Name, Text = rr.Name })
            //};

            var list = db.Roles.OrderBy(r => r.Name).ToList().Select(rr =>
                new SelectListItem { Value = rr.Name.ToString(), Text = rr.Name }).ToList();
            ViewBag.Roles = list;
            return View();
        }

        [System.Web.Mvc.HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CreateUserBindingModel userModel)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser()
                {
                    UserName = userModel.Email,
                    Email = userModel.Email,
                    //Roles = userModel.RoleName,
                    EmailConfirmed = true,
                    PhoneNumber = userModel.Telephone,
                    UserProfile = new UserProfile()
                    {
                        Name = userModel.Name,
                        CreatedAt = DateTime.UtcNow,
                        Address = userModel.Address
                    }
                };

                if (userModel.Photo != null && userModel.Photo != user.UserProfile.Photo)
                {
                    var rootPath = string.Format("/Content/uploads/{0}/", user.Id.ToString());
                    var directory = Server.MapPath(rootPath);

                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }

                    var path = rootPath + Path.GetFileName(userModel.Photo);
                    var ext = Path.GetExtension(userModel.Photo);
                    if (!_validImageTypes.Contains(ext))
                    {
                        return View(userModel);
                    }
                    string[] oldphoto = Directory.GetFiles(Server.MapPath(rootPath));
                    foreach (var old in oldphoto)
                    {
                        System.IO.File.Delete(old);
                    }
                    System.IO.File.Move(Server.MapPath(userModel.Photo), Server.MapPath(path));

                    user.UserProfile.Photo = path;
                }

                var generatedPassword = Membership.GeneratePassword(10, 2);
                var result = await this.AppUserManager.CreateAsync(user, generatedPassword);

                if (result.Succeeded)
                {
                    await AppUserManager.AddToRoleAsync(user.Id, userModel.RoleName);
                    
                    // @TODO kirim email konfirmasi
                    //var code = this.AppUserManager.GenerateEmailConfirmationToken(user.Id);
                    //string callbackUrl = Url.RouteUrl(
                    //    "ConfirmEmailRoute",
                    //    new { httproute = "", userId = user.Id, code = code },
                    //    protocol: Request.Url.Scheme
                    //);

                    //await this.AppUserManager.SendEmailAsync(user.Id, "Confirm your account",
                    //    "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                    //TempData["message"] =
                    //    string.Format(
                    //        "User has been successfully created, please confirm email by clicking the link send to {0}!Thanks.",
                    //        user.Email);

                    var request = Request;
                    var appUrl = HttpRuntime.AppDomainAppVirtualPath;
                    var baseUrl = string.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Authority, appUrl);
                    var callbackUrl = baseUrl + string.Format("Account/Login");

                    StreamReader reader = new StreamReader(HostingEnvironment.MapPath("~/Content/html_template/email_template.html"));

                    string readFile = reader.ReadToEnd();
                    string messageBody = "";

                    messageBody = readFile;
                    messageBody = messageBody.Replace("[username]", user.UserName)
                        .Replace("[password]", generatedPassword)
                        .Replace("[calbackurl]", callbackUrl);

                    this.AppUserManager.SendEmail(user.Id, "Welcome New Account!",
                        messageBody.ToString());

                    OnCrudOperation(new UserProfileEventArgs(user.UserProfile, CrudEventEnumStatus.Create));

                    if (!string.IsNullOrWhiteSpace(Request.QueryString["createNext"]))
                    {
                        if (Request.QueryString["createNext"] == "true")
                        {
                            TempData["Created"] = true;

                            return RedirectToAction("Create");
                        }
                    }

                    return RedirectToAction("Details", new { id = user.Id });
                }
                AddErrors(result);
            }
            var list = db.Roles.OrderBy(r => r.Name).ToList().Select(rr =>
                new SelectListItem { Value = rr.Name.ToString(), Text = rr.Name }).ToList();
            ViewBag.Roles = list;

            return View(userModel);
        }

        public ActionResult EditAccount(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = db.Users.Find(id);

            if (user == null)
            {
                return HttpNotFound();
            }

            ViewBag.Id = id;

            var editUserAccount = new EditUserAccountBindingModel()
            {
                Id = user.Id,
                Email = user.Email,
                Username = user.UserName
            };

            return View(editUserAccount);
        }

        [System.Web.Mvc.HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAccount(EditUserAccountBindingModel usermModel)
        {
            if (ModelState.IsValid)
            {
                var user = await this.AppUserManager.FindByIdAsync(usermModel.Id);
                user.Email = usermModel.Email;
                user.UserName = usermModel.Username;

                var userStore = new UserStore<ApplicationUser>(db);
                var userManager = new UserManager<ApplicationUser>(userStore);
                //var result = userManager.Update(user);
                var result = await this.AppUserManager.UpdateAsync(user);

                if (result.Succeeded)
                {
                    TempData["message"] =
                        string.Format(
                            "User account detail successfully updated");

                    return RedirectToAction("EditAccount", new { id = user.Id });
                }
                AddErrors(result);
            }

            return View(usermModel);
        }

        public ActionResult EditRole(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = db.Users.Find(id);

            if (user == null)
            {
                return HttpNotFound();
            }

            ViewBag.Id = id;
            ViewBag.Role = this.AppRoleManager.FindById(user.Roles.First().RoleId);
            var roles = db.Roles.OrderBy(r => r.Name).ToList().Select(rr =>
                new SelectListItem
                {
                    Value = rr.Name.ToString(),
                    Text = rr.Name
                }).ToList();

            ViewBag.Roles = roles;

            var editUserAccount = new EditUserAccountBindingModel()
            {
                Id = user.Id,
                Email = user.Email,
                Username = user.UserName
            };

            return View(editUserAccount);
        }

        [System.Web.Mvc.HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditRole(string userId, string roleName)
        {
            if (ModelState.IsValid)
            {
                //var user = await this.AppUserManager.FindByIdAsync(usermModel.Id);
                //user.Email = usermModel.Email;
                //user.UserName = usermModel.Username;

                //var userStore = new UserStore<ApplicationUser>(db);
                //var userManager = new UserManager<ApplicationUser>(userStore);
                ////var result = userManager.Update(user);
                //var result = await this.AppUserManager.UpdateAsync(user);

                //if (result.Succeeded)
                //{
                //    TempData["message"] =
                //        string.Format(
                //            "User account detail successfully updated");

                //    return RedirectToAction("EditAccount", new { id = user.Id });
                //}
                //AddErrors(result);
            }

            return View();
        }

        public ActionResult EditProfile(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var userProfile = db.UserProfiles.Find(id);

            if (userProfile == null)
            {
                return HttpNotFound();
            }

            ViewBag.Id = id;

            var user = db.Users.FirstOrDefault(x => x.UserProfile.Id == id);

            if (user == null)
            {
                return HttpNotFound();
            }

            var editUserProfile = new EditUserProfileBindingModel()
            {
                Id = userProfile.Id,
                UserId = user.Id,
                Name = userProfile.Name,
                Birthplace = userProfile.Birthplace,
                Birthdate = userProfile.Birthdate,
                Telephone = user.PhoneNumber,
                Address = userProfile.Address,
                Photo = userProfile.Photo
            };

            return View(editUserProfile);
        }

        [System.Web.Mvc.HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProfile(EditUserProfileBindingModel usermModel)
        {
            if (ModelState.IsValid)
            {
                var userProfile = db.UserProfiles.Find(usermModel.Id);
                userProfile.Name = usermModel.Name;
                userProfile.Birthplace = usermModel.Birthplace;
                userProfile.Birthdate = usermModel.Birthdate.HasValue && usermModel.Birthdate.Value.Year > 1800 ? usermModel.Birthdate : null;
                userProfile.Address = usermModel.Address;

                if (usermModel.Photo != null && usermModel.Photo != userProfile.Photo)
                {
                    var rootPath = string.Format("/Content/uploads/{0}/", userProfile.Id.ToString());
                    var directory = Server.MapPath(rootPath);

                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }

                    var path = rootPath + Path.GetFileName(usermModel.Photo);
                    var ext = Path.GetExtension(usermModel.Photo);
                    if (!_validImageTypes.Contains(ext))
                    {
                        return View(usermModel);
                    }
                    string[] oldphoto = Directory.GetFiles(Server.MapPath(rootPath));
                    foreach (var old in oldphoto)
                    {
                        System.IO.File.Delete(old);
                    }
                    System.IO.File.Move(Server.MapPath(usermModel.Photo), Server.MapPath(path));

                    userProfile.Photo = path;
                }

                db.Entry(userProfile).State = EntityState.Modified;

                db.SaveChanges();

                var user = this.AppUserManager.FindById(usermModel.UserId);
                user.PhoneNumber = usermModel.Telephone;
                this.AppUserManager.Update(user);

                TempData["message"] =
                        string.Format(
                            "User has been successfully edited");

                OnCrudOperation(new UserProfileEventArgs(userProfile, CrudEventEnumStatus.Update));
                return RedirectToAction("EditProfile", new { id = userProfile.Id });
            }

            return View(usermModel);
        }

        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var user = db.Users.Find(id);

            var userModel = new EditUserProfileBindingModel()
            {
                Id = user.UserProfile.Id,
                Name = user.UserProfile.Name,
                Birthplace = user.UserProfile.Birthplace,
                Birthdate = user.UserProfile.Birthdate,
                Address = user.UserProfile.Address,
            };

            ViewBag.Id = id;

            return View(userModel);
        }

        [System.Web.Mvc.HttpPost, System.Web.Mvc.ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            var user = db.Users.Find(id);
            ViewBag.Id = id;
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult ResetPassword(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var user = db.Users.Find(id);

            if (user == null)
            {
                return HttpNotFound();
            }

            ViewBag.Username = user.UserName;
            ViewBag.Email = user.Email;

            var model = new ResetPasswordModel()
            {
                Id = user.Id,
                Token = this.AppUserManager.GeneratePasswordResetToken(user.Id)
            };

            return View(model);
        }

        [System.Web.Mvc.HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                var result =  this.AppUserManager.ResetPassword(model.Id, model.Token, model.Password);

                if (result.Succeeded)
                {
                    TempData["message"] =
                        string.Format(
                            "Password successfully changed");

                    return RedirectToAction("ResetPassword");
                }
                AddErrors(result);
            }

            var user = this.AppUserManager.FindById(model.Id);

            ViewBag.Username = user.UserName;
            ViewBag.Email = user.Email;

            return View(model);
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult ActivateUser(string id)
        {
            var user = this.AppUserManager.FindById(id);

            if (user == null)
            {
                return HttpNotFound();
            }

            user.EmailConfirmed = true;

            var result = AppUserManager.Update(user);

            if (!result.Succeeded)
            {
                return Json(result.Errors, JsonRequestBehavior.DenyGet);
            }

            return Json(HttpStatusCode.OK, JsonRequestBehavior.DenyGet);
        }

        
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}