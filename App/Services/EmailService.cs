﻿using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace App.Services
{
    public class EmailService : IIdentityMessageService
    {
        public async Task SendAsync(IdentityMessage message)
        {
            await configSendasync(message);
        }

        private async Task configSendasync(IdentityMessage message)
        {
            var myMessage = new MailMessage(new MailAddress("sangku@sangkuriang.co.id", "Sangkuriang"),
                new MailAddress(message.Destination))
            {
                Subject = message.Subject,
                Body = message.Body,
                IsBodyHtml = true
            };

            var client = new SmtpClient();

            await client.SendMailAsync(myMessage);
        }
    }
}