namespace App.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addDerekKerjaDerekBengkelRekananCity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BengkelRekanan",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Address = c.String(),
                        Telephone = c.String(),
                        Fax = c.String(),
                        Email = c.String(),
                        ContactPerson = c.String(),
                        CityId = c.Int(nullable: false),
                        Description = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        LastUpdateTime = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.City", t => t.CityId, cascadeDelete: true)
                .Index(t => t.CityId);
            
            CreateTable(
                "dbo.City",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        LastUpdateTime = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Derek",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Telephone = c.String(nullable: false),
                        Email = c.String(),
                        EventDate = c.DateTime(nullable: false),
                        EventTime = c.DateTime(nullable: false),
                        Location = c.String(nullable: false),
                        CityId = c.Int(nullable: false),
                        Description = c.String(),
                        KerjaDerekId = c.Int(),
                        PemegangPolisId = c.Int(),
                        CreatedAt = c.DateTime(nullable: false),
                        LastUpdateTime = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.KerjaDerek", t => t.KerjaDerekId)
                .ForeignKey("dbo.PemegangPolis", t => t.PemegangPolisId)
                .Index(t => t.KerjaDerekId)
                .Index(t => t.PemegangPolisId);
            
            CreateTable(
                "dbo.KerjaDerek",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        LastUpdateTime = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AlterColumn("dbo.Klaim", "Keterangan", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Derek", "PemegangPolisId", "dbo.PemegangPolis");
            DropForeignKey("dbo.Derek", "KerjaDerekId", "dbo.KerjaDerek");
            DropForeignKey("dbo.BengkelRekanan", "CityId", "dbo.City");
            DropIndex("dbo.Derek", new[] { "PemegangPolisId" });
            DropIndex("dbo.Derek", new[] { "KerjaDerekId" });
            DropIndex("dbo.BengkelRekanan", new[] { "CityId" });
            AlterColumn("dbo.Klaim", "Keterangan", c => c.String());
            DropTable("dbo.KerjaDerek");
            DropTable("dbo.Derek");
            DropTable("dbo.City");
            DropTable("dbo.BengkelRekanan");
        }
    }
}
