namespace App.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changes : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PemegangPolis", "NomorPolis", c => c.String(nullable: false));
            AlterColumn("dbo.PemegangPolis", "NomorMesin", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PemegangPolis", "NomorMesin", c => c.String());
            AlterColumn("dbo.PemegangPolis", "NomorPolis", c => c.String());
        }
    }
}
