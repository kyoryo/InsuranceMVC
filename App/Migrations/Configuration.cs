using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using App.DAL;
using App.Infrastructure;
using Domain.Infrastructure;
using Domain.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace App.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "App.DAL.ApplicationDbContext";
        }

        protected override void Seed(ApplicationDbContext context)
        {

            var _kerjaKlaim = new List<KerjaKlaim>()
            {
                new KerjaKlaim {Name = "Draft"},
                new KerjaKlaim {Name = "In Progress"},
                new KerjaKlaim {Name = "Finished"},
                new KerjaKlaim {Name = "Cancelled"},
            };
            _kerjaKlaim.ForEach(s => context.KerjaKlaims.AddOrUpdate(p => p.Name, s));
            var _kerjaDerek = new List<KerjaDerek>()
            {
                new KerjaDerek {Name = "Draft"},
                new KerjaDerek {Name = "In Progress"},
                new KerjaDerek {Name = "Finished"},
                new KerjaDerek {Name = "Cancelled"},
            };
            _kerjaDerek.ForEach(s => context.KerjaDereks.AddOrUpdate(p => p.Name, s));


            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            var roleManager = new RoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(new ApplicationDbContext()));

            var user = new ApplicationUser()
            {
                UserName = "superadmin",
                Email = "sangku@sangkuriang.co.id",
                EmailConfirmed = true,
                UserProfile = new UserProfile()
                {
                    Name = "Super Admin",
                    Address = "Bandung",
                    Birthplace = "Bandung",
                    Birthdate = DateTime.UtcNow,
                    CreatedAt = DateTime.UtcNow
                }
            };

            

            manager.Create(user, "adminadmin");

            if (roleManager.Roles.Count() == 0)
            {
                roleManager.Create(new ApplicationRole { Name = "Admin" });
                roleManager.Create(new ApplicationRole { Name = "User" });
            }

            var adminUser = manager.FindByName("superadmin");

            manager.AddToRoles(adminUser.Id, new string[] { "Admin" });

            if (!context.Audiences.Any(x => x.ClientId == "33a6d4ceac1642fd8fa6b78651bf5f43"))
            {
                context.Audiences.Add(new Audience()
                {
                    Name = "Website",
                    ClientId = "33a6d4ceac1642fd8fa6b78651bf5f43",
                    Base64Secret = "8uIlEYxpzDHo_Tc5fIsdGjaXlhuWcA--T4_o_tnnDmQ",
                    CreatedAt = DateTime.UtcNow
                });
            }

            var admin = roleManager.FindByName("Admin");

            #region PoliceHolderInitial
            var _adminPolis = manager.FindByName("superadmin");
            var _pemegangPolis = new List<PemegangPolis>()
            {
                new PemegangPolis {NomorPolis = "Polis001", NomorMesin = "Mesin001", OwnerId = _adminPolis.Id, CreatedAt = DateTime.UtcNow },
                
            };
            _pemegangPolis.ForEach(s => context.PemegangPolis.AddOrUpdate(p => p.Id, s));
            #endregion

            if (!context.Menus.Any())
            {
                var roleMenus = new List<RoleMenu>()
                {
                    new RoleMenu()
                    {
                        Id = Guid.NewGuid(),
                        Menu = new Menu()
                        {
                            Id = Guid.NewGuid(),
                            Name = "Dashboard",
                            Action = "Index",
                            Controller = "Home",
                            Icon = "fa fa-home",
                            SortOrder = 0,
                            CreatedAt = DateTime.UtcNow,
                        },
                        CreatedAt = DateTime.UtcNow,
                        SortOrder = 0,
                        RoleId = admin.Id
                    },
                    new RoleMenu()
                    {
                        Id = Guid.NewGuid(),
                        Menu = new Menu()
                        {
                            Id = Guid.NewGuid(),
                            Name = "Audience",
                            Title = "Client Secret",
                            Action = "Index",
                            Controller = "Audience",
                            Icon = "fa fa-check-circle-o",
                            SortOrder = 1,
                            CreatedAt = DateTime.UtcNow,
                        },
                        CreatedAt = DateTime.UtcNow,
                        SortOrder = 1,
                        RoleId = admin.Id
                    },
                    new RoleMenu()
                    {
                        Id = Guid.NewGuid(),
                        Menu = new Menu()
                        {
                            Id = Guid.NewGuid(),
                            Name = "User Management",
                            Action = "Index",
                            Controller = "UserManagement",
                            Icon = "fa fa-users",
                            SortOrder = 2,
                            CreatedAt = DateTime.UtcNow,
                        },
                        CreatedAt = DateTime.UtcNow,
                        SortOrder = 2,
                        RoleId = admin.Id
                    },
                    new RoleMenu()
                    {
                        Id = Guid.NewGuid(),
                        Menu = new Menu()
                        {
                            Id = Guid.NewGuid(),
                            Name = "Role Management",
                            Icon = "fa fa-shield",
                            SortOrder = 3,
                            SubMenu = new List<SubMenu>()
                            {
                                new SubMenu(){Name = "Permission", SortOrder = 0, Controller = "Permission", Action = "Index"},
                                new SubMenu(){Name = "Role", SortOrder = 1, Controller = "Role", Action = "Index"}
                            },
                            CreatedAt = DateTime.UtcNow,
                        },
                        CreatedAt = DateTime.UtcNow,
                        SortOrder = 3,
                        RoleId = admin.Id
                    },
                    new RoleMenu()
                    {
                        Id = Guid.NewGuid(),
                        Menu = new Menu()
                        {
                            Id = Guid.NewGuid(),
                            Name = "Master Data",
                            Icon = "fa fa-cogs",
                            SortOrder = 4,
                            SubMenu = new List<SubMenu>()
                            {
                                new SubMenu(){Name = "Category", SortOrder = 0, Controller = "Category", Action = "Index"},
                                new SubMenu(){Name = "Menu", SortOrder = 1, Controller = "Menu", Action = "Index"},
                                new SubMenu(){Name = "Product", SortOrder = 2, Controller = "Product", Action = "Index"},
                                new SubMenu(){Name = "City", SortOrder = 3, Controller = "City", Action = "Index"},
                                new SubMenu(){Name = "Pemegang Polis", SortOrder = 4, Controller = "PemegangPolis", Action = "Index"},
                                new SubMenu(){Name = "Kerja Klaim", SortOrder = 5, Controller = "KerjaKlaim", Action = "Index"},
                                new SubMenu(){Name = "Kerja Derek", SortOrder = 6, Controller = "KerjaDerek", Action = "Index"}
                            },
                            CreatedAt = DateTime.UtcNow,
                        },
                        CreatedAt = DateTime.UtcNow,
                        SortOrder = 4,
                        RoleId = admin.Id
                    },
                    #region new
                    new RoleMenu()
                    {
                        Id = Guid.NewGuid(),
                        Menu = new Menu()
                        {
                            Id = Guid.NewGuid(),
                            Name = "User Data",
                            Icon = "fa fa-cog",
                            SortOrder = 5,
                            SubMenu = new List<SubMenu>()
                            {
                                new SubMenu(){Name = "Klaim", SortOrder = 0, Controller = "Klaim", Action = "Index"},
                                new SubMenu(){Name = "Derek", SortOrder = 1, Controller = "Derek", Action = "Index"},
                                new SubMenu(){Name = "Bengkel", SortOrder = 2, Controller = "BengkelRekanan", Action = "Index"},
                                new SubMenu(){Name = "Bantuan", SortOrder = 3},
                            },
                            CreatedAt = DateTime.UtcNow,
                        },
                        CreatedAt = DateTime.UtcNow,
                        SortOrder = 5,
                        RoleId = admin.Id
                    },
                    #endregion
                };

                foreach (var roleMenu in roleMenus)
                {
                    context.RoleMenus.Add(roleMenu);
                }
            }

            if (!context.Permissions.Any())
            {
                var rolePermissions = new List<RolePermission>()
                {
                    new RolePermission()
                    {
                        Id = Guid.NewGuid(),
                        CreatedAt = DateTime.UtcNow,
                        Permission = new Permission()
                        {
                            Id = Guid.NewGuid(),
                            CreatedAt = DateTime.UtcNow,
                            Name = "administrator_only"
                        },
                        RoleId = admin.Id
                    },
                    new RolePermission()
                    {
                        Id = Guid.NewGuid(),
                        CreatedAt = DateTime.UtcNow,
                        Permission = new Permission()
                        {
                            Id = Guid.NewGuid(),
                            CreatedAt = DateTime.UtcNow,
                            Name = "crud_master_data"
                        },
                        RoleId = admin.Id
                    },
                    new RolePermission()
                    {
                        Id = Guid.NewGuid(),
                        CreatedAt = DateTime.UtcNow,
                        Permission = new Permission()
                        {
                            Id = Guid.NewGuid(),
                            CreatedAt = DateTime.UtcNow,
                            Name = "read_master_data"
                        },
                        RoleId = admin.Id
                    },
                    new RolePermission()
                    {
                        Id = Guid.NewGuid(),
                        CreatedAt = DateTime.UtcNow,
                        Permission = new Permission()
                        {
                            Id = Guid.NewGuid(),
                            CreatedAt = DateTime.UtcNow,
                            Name = "user_crud"
                        },
                        RoleId = admin.Id
                    },
                    new RolePermission()
                    {
                        Id = Guid.NewGuid(),
                        CreatedAt = DateTime.UtcNow,
                        Permission = new Permission()
                        {
                            Id = Guid.NewGuid(),
                            CreatedAt = DateTime.UtcNow,
                            Name = "user_read_only"
                        },
                        RoleId = admin.Id
                    }
                };

                foreach (var rolePermission in rolePermissions)
                {
                    context.RolePermissions.Add(rolePermission);
                }
            }
           
            context.SaveChanges("Migration");
        }
    }
}