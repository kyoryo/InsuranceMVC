namespace App.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addPemegangPolisKlaimKerjaKlaim : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.KerjaKlaim",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CreatedAt = c.DateTime(nullable: false),
                        LastUpdateTime = c.DateTime(),
                        Name = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Klaim",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NamaPengemudi = c.String(nullable: false),
                        NomorTelpon = c.String(nullable: false),
                        Email = c.String(),
                        TanggalKejadian = c.DateTime(nullable: false),
                        WaktuKejadian = c.DateTime(nullable: false),
                        LokasiKejadian = c.String(nullable: false),
                        Keterangan = c.String(),
                        KerjaKlaimId = c.Int(),
                        PemegangPolisId = c.Int(),
                        CreatedAt = c.DateTime(nullable: false),
                        LastUpdateTime = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.KerjaKlaim", t => t.KerjaKlaimId)
                .ForeignKey("dbo.PemegangPolis", t => t.PemegangPolisId)
                .Index(t => t.KerjaKlaimId)
                .Index(t => t.PemegangPolisId);
            
            CreateTable(
                "dbo.PemegangPolis",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OwnerId = c.String(maxLength: 128),
                        NomorPolis = c.String(),
                        NomorMesin = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        LastUpdateTime = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.OwnerId)
                .Index(t => t.OwnerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Klaim", "PemegangPolisId", "dbo.PemegangPolis");
            DropForeignKey("dbo.PemegangPolis", "OwnerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Klaim", "KerjaKlaimId", "dbo.KerjaKlaim");
            DropIndex("dbo.PemegangPolis", new[] { "OwnerId" });
            DropIndex("dbo.Klaim", new[] { "PemegangPolisId" });
            DropIndex("dbo.Klaim", new[] { "KerjaKlaimId" });
            DropTable("dbo.PemegangPolis");
            DropTable("dbo.Klaim");
            DropTable("dbo.KerjaKlaim");
        }
    }
}
