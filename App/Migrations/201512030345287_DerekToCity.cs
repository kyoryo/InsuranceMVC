namespace App.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DerekToCity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BranchOffice", "CityId", c => c.Int(nullable: false));
            CreateIndex("dbo.BranchOffice", "CityId");
            CreateIndex("dbo.Derek", "CityId");
            AddForeignKey("dbo.BranchOffice", "CityId", "dbo.City", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Derek", "CityId", "dbo.City", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Derek", "CityId", "dbo.City");
            DropForeignKey("dbo.BranchOffice", "CityId", "dbo.City");
            DropIndex("dbo.Derek", new[] { "CityId" });
            DropIndex("dbo.BranchOffice", new[] { "CityId" });
            DropColumn("dbo.BranchOffice", "CityId");
        }
    }
}
