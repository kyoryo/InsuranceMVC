﻿
using System.Web;
using System.Web.Mvc;

namespace App.Infrastructure
{
    public class ControllerAuthorizeAttribute : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(AuthorizationContext context)
        {
            if (context.HttpContext.Request.IsAuthenticated)
            {
                throw new HttpException((int)System.Net.HttpStatusCode.Forbidden, "Forbidden");
            }
            else
            {
                base.HandleUnauthorizedRequest(context);
            }
        }
    }
}