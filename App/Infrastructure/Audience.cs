﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography;
using App.DAL;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Scaffold;
using Scaffold.Models.Interface;

namespace App.Infrastructure
{
    [TrackChanges()]
    public class Audience : IDatedEntity
    {
        [Key]
        [MaxLength(32)]
        [ToTS(TSFlag.SkipIndex)]
        public string ClientId { get; set; }

        [MaxLength(80)]
        [Required]
        [ToTS(TSFlag.SkipIndex)]
        public string Base64Secret { get; set; }

        [MaxLength(100)]
        [Required]
        [DisplayName("Issuer")]
        public string Name { get; set; }

        [ToTS(TSFlag.SkipCRUD)]
        [SkipTracking()]
        public DateTime CreatedAt { get; set; }

        [ToTS(TSFlag.SkipCRUD)]
        [SkipTracking()]
        public DateTime? LastUpdateTime { get; set; }
    }

    public class AudienceModel
    {
        [MaxLength(100)]
        [Required]
        [DisplayName("Issuer")]
        public string Name { get; set; }
    }

    public static class AudiencesStore
    {
        public static Audience AddAudience(string name)
        {
            using (var db = new ApplicationDbContext())
            {
                var clientId = Guid.NewGuid().ToString("N");

                var key = new byte[32];
                RNGCryptoServiceProvider.Create().GetBytes(key);
                var base64Secret = TextEncodings.Base64Url.Encode(key);

                var newAudience = new Audience { ClientId = clientId, Base64Secret = base64Secret, Name = name };

                db.Audiences.Add(newAudience);
                db.SaveChanges("System");

                return newAudience;

            }
        }

        public static Audience FindAudience(string clientId)
        {
            using (var db = new ApplicationDbContext())
            {
                var audience = db.Audiences.Find(clientId);
                return audience;
            }
        }
    }
}