﻿using App.Enums;

namespace App.Infrastructure
{
    public interface IEventArgs<T> where T : class
    {
        CrudEventEnumStatus Status { get; set; }
        T After { get; set; }
    }
}