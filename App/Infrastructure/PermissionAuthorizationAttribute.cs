﻿using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using App.Services;
using Microsoft.AspNet.Identity;

namespace App.Infrastructure
{
    public class PermissionAuthorizationAttribute : AuthorizeAttribute
    { 
        private readonly IAccountService _accountService;
        private readonly IEnumerable<string> _permissions;

        public PermissionAuthorizationAttribute(params string[] permissions)
        {
            _accountService = new AccountService();
            _permissions = permissions;
        }

        protected PermissionAuthorizationAttribute(IAccountService accountService)
        {
            _accountService = accountService;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (!_permissions.Any(x => _accountService.HasPermission(filterContext.HttpContext.User.Identity.GetUserId(), x)))
                throw new HttpException((int)System.Net.HttpStatusCode.Forbidden, "Forbidden");
            base.OnAuthorization(filterContext);
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext context)
        {
            if (context.HttpContext.Request.IsAuthenticated)
            {
                throw new HttpException((int)System.Net.HttpStatusCode.Forbidden, "Forbidden");
            }
            else
            {
                base.HandleUnauthorizedRequest(context);
            }
        }
    }
}