﻿var config = {
    baseUrl: "http://localhost:12314/"
}

function setAjaxUseAuth() {
    $.ajaxSetup({
        headers: { 'Authorization': "bearer " + $.cookie("auth") }
    });
}

var $toggleModal = function () {
    $('[data-toggle="modal"]').unbind().on('click', function (e) {
        e.preventDefault();
        var modalTitle = $(this).data("modal-title");
        var url = $(this).data('href');
        if (!url.indexOf('#') == 0) {
            var l = Ladda.create(this);

            l.start();

            $.get(url, function (data) {
                $("#modal-confirm .modal-body").html(data);
                $("#modal-confirm .modal-title").html(modalTitle);
                $("#modal-confirm").modal('show');
                l.stop();
            });
        }
    });
}

$toggleModal();

function convertUTCDateToLocalDate(date) {
    var newDate = new Date(date.getTime() + date.getTimezoneOffset() * 60 * 1000);

    var offset = date.getTimezoneOffset() / 60;
    var hours = date.getHours();

    newDate.setHours(hours - offset);

    return newDate;
}

$(function () {
    $("ul[data-navigation='true']").find("li").each(function () {
        if ($(this).hasClass("active")) {
            $(this).parent().parent().addClass("navActive active");
        }
    });
});

$('#search-input').keyup($.debounce(500, searchingKey));

function searchingKey() {
    elasticSearch($(this).val());
}

$("form[name='search']").submit(function (e) {
    e.preventDefault();

    var q = $('#search-input').val();
    //console.log(q);
    elasticSearch(q);
});

function elasticSearch(q) {
    $.ajax({
        url: config.baseUrl + "elastic/search?q=*" + q + "*",
        type: "GET",
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            var result = data;
            var content = "";

            $.each(JSON.parse(result.Users), function (index, item) {
                content += " <a class='dummy-media-object' href='/Member/Detail/" + item._source.Id + "'>" +
                    "<img src='/Content/make/assets/global/images/avatars/avatar1_big.png' alt='Avatar 1' />" +
                    "<h3>" + item._source.FullName + "</h3>" +
                    "</a>";
            });

            $("#listUser").html(content);

            content = '';

            $.each(JSON.parse(result.All), function (index, item) {
                content += renderSearch(item, q);
            });

            $("#listArticle").html(content);

            content = '';

            $.each(JSON.parse(result.Recents), function (index, item) {
                content += renderSearch(item, q);
            });

            $("#listRecent").html(content);
        }
    });
}

function renderSearch(item, q) {
    var content = '';
    var title = item._source.Name !== undefined ? item._source.Name : item._source.Title;
    if (item._source.Description !== undefined || item._source.Content !== undefined) {
        var same = item._source.Description !== undefined ? item._source.Description : item._source.Content;
        var samesame = same.indexOf(q);
        var l = q.length;
        var start = samesame + l;
        var before = same.substring(start - 50, start);
        var limit = samesame + 100;
        var get = same.substring(start, limit);
        content += " <a class='dummy-media-object' href='/" + item._type + "/details/" + item._source.Id + "'>" +
            "<h3 style='color:#319DB5'>" + title + "</h3>" +
            "<p style='color:#fff'>..." + before + "<span style='color:#319DB5;font-weight:bold;'>" + q + "</span>" + get + "...</p>" +
            "<p>" + item._type + "</p>" +
            "</a>";
    } else {
        content += " <a class='dummy-media-object' href='/" + item._type + "/details/" + item._source.Id + "'>" +
            "<h3 style='color:#319DB5'>" + title + "</h3>" +

            "<p>" + item._type + "</p>" +
            "</a>";
    }

    return content;
}

var firstCall = true;

$("#search-results input").bind("focus", function () {
    if (firstCall) {
        elasticSearch("");
        firstCall = false;
    }
});

function Truncate(string, take) {
    return string.length > take ? string.substring(0, take) + "..." : string;
}

function CreateEditor(selector) {
    $(selector).kendoEditor({
        tools: [
            "bold",
            "italic",
            "underline",
            "strikethrough",
            "justifyLeft",
            "justifyCenter",
            "justifyRight",
            "justifyFull",
            "insertUnorderedList",
            "insertOrderedList",
            "indent",
            "outdent",
            "createLink",
            "unlink",
            "insertImage",
            "insertFile",
            "subscript",
            "superscript",
            "createTable",
            "addRowAbove",
            "addRowBelow",
            "addColumnLeft",
            "addColumnRight",
            "deleteRow",
            "deleteColumn",
            "viewHtml",
            "formatting",
            "cleanFormatting",
            "fontName",
            "fontSize",
            "foreColor",
            "backColor"
        ],
        encoded: false
    });
}

function CreateDateTimePicker(selector) {
    $(selector).kendoDateTimePicker({
        format: "yyyy-MM-dd HH:mm"
    });
}

function CreateDropDownList(selector, filter, dataSource, dataTextField, dataTextValue, value) {
    var options = {
        filter: filter,
        dataSource: dataSource,
        dataTextField: dataTextField,
        dataValueField: dataTextValue
    };

    if (value !== null) {
        options.value = value;
    }

    $(selector).kendoDropDownList(options);
}

function CreateComboBox(selector, filter, dataSource, dataTextField, dataTextValue, value) {
    var options = {
        filter: filter,
        dataSource: dataSource,
        dataTextField: dataTextField,
        dataValueField: dataTextValue
    };

    if (value !== null) {
        options.value = value;
    }

    $(selector).kendoComboBox(options);
}