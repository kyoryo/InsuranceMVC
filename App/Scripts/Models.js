﻿
// WARNING: T4 generated file  (it is related to CodeToServerProxy)
//

/// <reference path="kendo/kendo.all.min.js"/>

var App;
(function (App) {
    var Models;
    (function (Models) {
        var Category = (function (){
            function Category(){};
            Category.Model = {
                id: 'Id',
                fields: {
                    Id : {
                        type : "number"
                    },
                    Name : {
                        type : "string"
                    },
                    Description : {
                        type : "string"
                    },
                    CreatedAt : {
                        type : "date"
                    },
                    LastUpdateTime : {
                        type : "date"
                    },
                }
            }

            Category.DsOptions = {
                type: "json",
                transport: {
                    read: {
                        url: "/api/Categories",
                        dataType: "json",
                        contentType: "application/json"
                    },
                    destroy: {
                        url: function (data) {
                            return "/api/Categories/" + data.Id;
                        },
                        type: "delete"
                    },
                    parameterMap: function (options, operation) {
                        if (operation === "destroy") {
                            return null;
                        } else if (operation !== "read" && options.models) {
                            return { models: kendo.stringify(options.models) };
                        } else {
                            return "q=" + JSON.stringify(options);
                        }
                    }
                },
                schema: {
                    data: "Data",
                    total: "Total",
                    model: Category.Model
                },
                batch: false,
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                pageSize: 10
            }

            Category.DataSource = function (options, extend) {
                if (options) {
                    if (extend)
                        return new kendo.data.DataSource($.extend(Category.DsOptions, options));
                    else
                        return new kendo.data.DataSource(options);
                }
                return new kendo.data.DataSource(Category.DsOptions);
            }

            return Category;
        })();
        Models.Category = Category;

        var Product = (function (){
            function Product(){};
            Product.Model = {
                id: 'Id',
                fields: {
                    Id : {
                        type : "guid"
                    },
                    Name : {
                        type : "string"
                    },
                    Picture : {
                        type : "string"
                    },
                    Description : {
                        type : "string"
                    },
                    Price : {
                        type : "number"
                    },
                    Quantity : {
                        type : "number"
                    },
                    CategoryId : {
                        type : "number"
                    },
                    CreatedAt : {
                        type : "date"
                    },
                    LastUpdateTime : {
                        type : "date"
                    },
                }
            }

            Product.DsOptions = {
                type: "json",
                transport: {
                    read: {
                        url: "/api/Products",
                        dataType: "json",
                        contentType: "application/json"
                    },
                    destroy: {
                        url: function (data) {
                            return "/api/Products/" + data.Id;
                        },
                        type: "delete"
                    },
                    parameterMap: function (options, operation) {
                        if (operation === "destroy") {
                            return null;
                        } else if (operation !== "read" && options.models) {
                            return { models: kendo.stringify(options.models) };
                        } else {
                            return "q=" + JSON.stringify(options);
                        }
                    }
                },
                schema: {
                    data: "Data",
                    total: "Total",
                    model: Product.Model
                },
                batch: false,
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                pageSize: 10
            }

            Product.DataSource = function (options, extend) {
                if (options) {
                    if (extend)
                        return new kendo.data.DataSource($.extend(Product.DsOptions, options));
                    else
                        return new kendo.data.DataSource(options);
                }
                return new kendo.data.DataSource(Product.DsOptions);
            }

            return Product;
        })();
        Models.Product = Product;

        var UserProfile = (function (){
            function UserProfile(){};
            UserProfile.Model = {
                id: 'Id',
                fields: {
                    Id : {
                        type : "number"
                    },
                    Name : {
                        type : "string"
                    },
                    Birthplace : {
                        type : "string"
                    },
                    Birthdate : {
                        type : "date"
                    },
                    Address : {
                        type : "string"
                    },
                    Photo : {
                        type : "string"
                    },
                    CreatedAt : {
                        type : "date"
                    },
                    LastUpdateTime : {
                        type : "date"
                    },
                    ApplicationUser : {
                        type : "ApplicationUser"
                    },
                }
            }

            UserProfile.DsOptions = {
                type: "json",
                transport: {
                    read: {
                        url: "/api/UserProfiles",
                        dataType: "json",
                        contentType: "application/json"
                    },
                    destroy: {
                        url: function (data) {
                            return "/api/UserProfiles/" + data.Id;
                        },
                        type: "delete"
                    },
                    parameterMap: function (options, operation) {
                        if (operation === "destroy") {
                            return null;
                        } else if (operation !== "read" && options.models) {
                            return { models: kendo.stringify(options.models) };
                        } else {
                            return "q=" + JSON.stringify(options);
                        }
                    }
                },
                schema: {
                    data: "Data",
                    total: "Total",
                    model: UserProfile.Model
                },
                batch: false,
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                pageSize: 10
            }

            UserProfile.DataSource = function (options, extend) {
                if (options) {
                    if (extend)
                        return new kendo.data.DataSource($.extend(UserProfile.DsOptions, options));
                    else
                        return new kendo.data.DataSource(options);
                }
                return new kendo.data.DataSource(UserProfile.DsOptions);
            }

            return UserProfile;
        })();
        Models.UserProfile = UserProfile;

        var Menu = (function (){
            function Menu(){};
            Menu.Model = {
                id: 'Id',
                fields: {
                    Id : {
                        type : "guid"
                    },
                    Name : {
                        type : "string"
                    },
                    Title : {
                        type : "string"
                    },
                    Action : {
                        type : "string"
                    },
                    Controller : {
                        type : "string"
                    },
                    Url : {
                        type : "string"
                    },
                    Icon : {
                        type : "string"
                    },
                    SubMenuJson : {
                        type : "string"
                    },
                    SubMenu : {
                        type : "SubMenu>"
                    },
                    CreatedAt : {
                        type : "date"
                    },
                    LastUpdateTime : {
                        type : "date"
                    },
                    SortOrder : {
                        type : "number"
                    },
                }
            }

            Menu.DsOptions = {
                type: "json",
                transport: {
                    read: {
                        url: "/api/Menus",
                        dataType: "json",
                        contentType: "application/json"
                    },
                    destroy: {
                        url: function (data) {
                            return "/api/Menus/" + data.Id;
                        },
                        type: "delete"
                    },
                    parameterMap: function (options, operation) {
                        if (operation === "destroy") {
                            return null;
                        } else if (operation !== "read" && options.models) {
                            return { models: kendo.stringify(options.models) };
                        } else {
                            return "q=" + JSON.stringify(options);
                        }
                    }
                },
                schema: {
                    data: "Data",
                    total: "Total",
                    model: Menu.Model
                },
                batch: false,
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                pageSize: 10
            }

            Menu.DataSource = function (options, extend) {
                if (options) {
                    if (extend)
                        return new kendo.data.DataSource($.extend(Menu.DsOptions, options));
                    else
                        return new kendo.data.DataSource(options);
                }
                return new kendo.data.DataSource(Menu.DsOptions);
            }

            return Menu;
        })();
        Models.Menu = Menu;

        var Permission = (function (){
            function Permission(){};
            Permission.Model = {
                id: 'Id',
                fields: {
                    Id : {
                        type : "guid"
                    },
                    Name : {
                        type : "string"
                    },
                    Description : {
                        type : "string"
                    },
                    CreatedAt : {
                        type : "date"
                    },
                    LastUpdateTime : {
                        type : "date"
                    },
                }
            }

            Permission.DsOptions = {
                type: "json",
                transport: {
                    read: {
                        url: "/api/Permissions",
                        dataType: "json",
                        contentType: "application/json"
                    },
                    destroy: {
                        url: function (data) {
                            return "/api/Permissions/" + data.Id;
                        },
                        type: "delete"
                    },
                    parameterMap: function (options, operation) {
                        if (operation === "destroy") {
                            return null;
                        } else if (operation !== "read" && options.models) {
                            return { models: kendo.stringify(options.models) };
                        } else {
                            return "q=" + JSON.stringify(options);
                        }
                    }
                },
                schema: {
                    data: "Data",
                    total: "Total",
                    model: Permission.Model
                },
                batch: false,
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                pageSize: 10
            }

            Permission.DataSource = function (options, extend) {
                if (options) {
                    if (extend)
                        return new kendo.data.DataSource($.extend(Permission.DsOptions, options));
                    else
                        return new kendo.data.DataSource(options);
                }
                return new kendo.data.DataSource(Permission.DsOptions);
            }

            return Permission;
        })();
        Models.Permission = Permission;

        var RoleMenu = (function (){
            function RoleMenu(){};
            RoleMenu.Model = {
                id: 'Id',
                fields: {
                    Id : {
                        type : "guid"
                    },
                    CreatedAt : {
                        type : "date"
                    },
                    LastUpdateTime : {
                        type : "date"
                    },
                    SortOrder : {
                        type : "number"
                    },
                    RoleId : {
                        type : "string"
                    },
                    MenuId : {
                        type : "guid"
                    },
                }
            }

            RoleMenu.DsOptions = {
                type: "json",
                transport: {
                    read: {
                        url: "/api/RoleMenus",
                        dataType: "json",
                        contentType: "application/json"
                    },
                    destroy: {
                        url: function (data) {
                            return "/api/RoleMenus/" + data.Id;
                        },
                        type: "delete"
                    },
                    parameterMap: function (options, operation) {
                        if (operation === "destroy") {
                            return null;
                        } else if (operation !== "read" && options.models) {
                            return { models: kendo.stringify(options.models) };
                        } else {
                            return "q=" + JSON.stringify(options);
                        }
                    }
                },
                schema: {
                    data: "Data",
                    total: "Total",
                    model: RoleMenu.Model
                },
                batch: false,
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                pageSize: 10
            }

            RoleMenu.DataSource = function (options, extend) {
                if (options) {
                    if (extend)
                        return new kendo.data.DataSource($.extend(RoleMenu.DsOptions, options));
                    else
                        return new kendo.data.DataSource(options);
                }
                return new kendo.data.DataSource(RoleMenu.DsOptions);
            }

            return RoleMenu;
        })();
        Models.RoleMenu = RoleMenu;

        var RolePermission = (function (){
            function RolePermission(){};
            RolePermission.Model = {
                id: 'Id',
                fields: {
                    Id : {
                        type : "guid"
                    },
                    CreatedAt : {
                        type : "date"
                    },
                    LastUpdateTime : {
                        type : "date"
                    },
                    RoleId : {
                        type : "string"
                    },
                    PermissionId : {
                        type : "guid"
                    },
                }
            }

            RolePermission.DsOptions = {
                type: "json",
                transport: {
                    read: {
                        url: "/api/RolePermissions",
                        dataType: "json",
                        contentType: "application/json"
                    },
                    destroy: {
                        url: function (data) {
                            return "/api/RolePermissions/" + data.Id;
                        },
                        type: "delete"
                    },
                    parameterMap: function (options, operation) {
                        if (operation === "destroy") {
                            return null;
                        } else if (operation !== "read" && options.models) {
                            return { models: kendo.stringify(options.models) };
                        } else {
                            return "q=" + JSON.stringify(options);
                        }
                    }
                },
                schema: {
                    data: "Data",
                    total: "Total",
                    model: RolePermission.Model
                },
                batch: false,
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                pageSize: 10
            }

            RolePermission.DataSource = function (options, extend) {
                if (options) {
                    if (extend)
                        return new kendo.data.DataSource($.extend(RolePermission.DsOptions, options));
                    else
                        return new kendo.data.DataSource(options);
                }
                return new kendo.data.DataSource(RolePermission.DsOptions);
            }

            return RolePermission;
        })();
        Models.RolePermission = RolePermission;

    })(Models || (Models = {}));
    App.Models = Models;
})(App || (App = {}));

